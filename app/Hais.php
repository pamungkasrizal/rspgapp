<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class Hais extends Model
{
    use Notifiable, HasRoles;
	
	public $incrementing = false;
	protected $primaryKey = 'no_reg';
	
	protected $casts = [
		'data_kunjungan' => 'array',
		'data_medis' => 'array',
		'data_pemasangan' => 'array',
		'data_irs' => 'array',
		'data_user' => 'array'
	];
	
	 protected $fillable = [
        'no_reg', 'data_kunjungan', 'data_medis', 'data_pemasangan', 'data_irs', 'data_user'
    ];
}
