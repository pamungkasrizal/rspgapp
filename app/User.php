<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\Uuid;

class User extends Authenticatable
{
    use Notifiable, HasRoles, Uuid;
	
	public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $casts = [
		'json_user' => 'array',
		'json_data' => 'array',
	];
	
    protected $fillable = [
        'id', 'name', 'nip', 'password', 'id_direktorat', 'id_bagian', 'id_subagseksi', 'id_instalasi', 'json_user', 'json_data'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
	
	public function instalasi()
    {
		$data = $this->belongsTo('App\Instalasi', 'id_instalasi', 'id');
		return $data;
    }
	
	public function subagseksi()
    {
		$data = $this->belongsTo('App\Subagseksi', 'id_subagseksi', 'id');
		return $data;
    }
	
	public function bagian()
    {
		$data = $this->belongsTo('App\Bagian', 'id_bagian', 'id');
		return $data;
    }
	
	public function direktorat()
    {
        $data = $this->belongsTo('App\GlobalParams', 'id_direktorat', 'sq_id');
		$data->getQuery()->where('xcond', 'direktorat');
		return $data;
    }
}
