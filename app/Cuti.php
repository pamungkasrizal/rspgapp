<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class Cuti extends Model
{
    use Notifiable, HasRoles;
	
	protected $casts = [
		'implementer' => 'array',
		'head' => 'array',
		'json_data' => 'array'
	];
	
    protected $fillable = [
        'id_user', 'type', 'shift', 'start_date', 'end_date', 'implementer', 'head', 'json_data', 'qty', 'reason', 'comp_name', 'ip_address'
    ];
	
	protected $hidden = [
		'comp_name', 'ip_address'
	];
	
	public function user()
    {
        $data = $this->belongsTo('App\User', 'id_user', 'id');
		$data->getQuery()->select('id', 'name', 'nip', 'json_data');
		return $data;
    }
}
