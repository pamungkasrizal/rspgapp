<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalParams extends Model
{
	protected $casts = [
		'json_data' => 'array'
	];
	
    protected $fillable = [
        'xcond', 'sq_id', 'char_id', 'description',
    ];
	
	public function bagian()
    {
        $data = $this->hasMany('App\Bagian', 'id_direktorat', 'sq_id');
		return $data;
    }
}
