<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
    protected $table = 'master';
	
    protected $fillable = ['name', 'description'];
}
