<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    protected $connection = 'sirs';
    protected $table = 'kd_dr';
	
	protected $fillable = [
        'kd_dr', 'nama_dr', 'gelar_dpn', 'gelar_blk', 'keahlian', 'aktif', 'nip'
    ];
	
	protected $hidden = [
        'alamat_praktek', 'kota_praktek', 'telp_praktek', 'alamat_rmh', 'kota_rmh', 'telpon_rmh', 'cellular', 'pager', 'kd_perk', 'no_peg', 'npwp', 'tgl_update', 'status', 'no_peg_dr', 'nama_dr2'
    ];
}
