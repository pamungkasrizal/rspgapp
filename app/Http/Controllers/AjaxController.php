<?php

namespace App\Http\Controllers;

use App\Authorizable;
use App\GlobalParams;
use App\Bagian;
use App\Subagseksi;
use App\Instalasi;
use App\Barang;
use App\Registrasi;
use App\Hais;
use App\Cuti;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use DB;

class AjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        /* if($request->ajax()){

			return true;
		}

		die(response()->json(['error' => 'Unauthenticated.'], 401)); */
	}
	
	public function getBagian(Request $request)
	{
		$bagian = Bagian::where('id_direktorat', $request->term)
						->orderBy('name', 'asc')
						->pluck('name', 'id');
		$html = Helper::selectboxOption($bagian, '-- Pilih Bidang/Bagian --', 'Kepala', false);				
		return $html;
	}
	
	public function getSubagSeksi(Request $request)
	{
		$subagseksi = Subagseksi::where([
							['id_direktorat', $request->direk], 
							['id_bagian', $request->term]
						])
						->orderBy('name', 'asc')
						->pluck('name', 'id');
		$html = Helper::selectboxOption($subagseksi, '-- Pilih Seksi/SubBagian --', 'Kepala', 'Staff', true);
		return $html;
	}
	
	public function getInstalasi(Request $request)
	{
		$instalasi = Instalasi::where([
							['id_direktorat', $request->direk], 
							['id_bagian', $request->bagian],
							['id_subagseksi', $request->term]
						])
						->orderBy('name', 'asc')
						->pluck('name', 'id');
		$html = Helper::selectboxOption2($instalasi, '-- Pilih Instalasi/Ruangan --', 'Kepala', 'Staff');
		return $html;
	}
	
	public function getBarangFarmasi(Request $request)
	{
		$term = ucwords(strtolower($request->term));
		//$barang = Barang::select('kode_barang as id', DB::raw("nama_barang || ' @ ' || ROUND(sum(qtybg), 0) as name"))
		$barang = Barang::select('kode_barang as id', 'nama_barang as name')
						->join('mfifo', 'm_barang.kode_barang', '=', 'mfifo.kdbrg')
						->where([
							['nama_barang', 'LIKE', '%'.$term.'%'],
							['tipe', 'F'],
							['qtybg', '>', 0]
						])
						->orderBy('nama_barang', 'asc')
						->groupBy('kode_barang','nama_barang')
						->limit(5)
						->get();
						
		die(json_encode($barang));
	}
	
	public function getDataPasien(Request $request)
	{
		$conditions = [];
		$term = strtoupper($request->term);
		//if($request->closed) $conditions[] = ['closed', $request->closed];
		if($request->inap) $conditions[] = ['inap', $request->inap];
		
		$data = Registrasi::select('no_reg', DB::raw("no_mr || ' | ' || no_reg || ' | ' || nama_pas as name"), 'no_mr', 'nama_pas', 'tgl_lahir', 'lp', 'tgl_reg', 'ds_icd_masuk', 'ds_dr', 'ds_dep', 'tgl_pulang')
					->where('no_mr', 'LIKE', '%'.$term.'%')
					->orWhere('no_reg', 'LIKE', '%'.$term.'%')
					->orWhere('nama_pas', 'LIKE', '%'.$term.'%')
					->where($conditions)
					->orderBy('tgl_reg', 'desc')
					->limit(10)
					->get();
					
		die(json_encode($data));
	}
	
	public function getICD(Request $request)
	{
		$term = strtoupper($request->term);
		
		$data = DB::connection('sirs')->table('kd_icd')
					->select('kode_icd', DB::raw("kode_icd || ' | ' || desk_icd as name"))
					->where('kode_icd', 'LIKE', '%'.$term.'%')
					->orWhere('desk_icd', 'LIKE', '%'.$term.'%')
					->orderBy('desk_icd', 'asc')
					->limit(10)
					->get();
					
		die(json_encode($data));
	}
	
	public function getDataHAIs(Request $request)
	{		
		$data = Hais::where('no_reg', trim($request->term))->first();
					
		die(json_encode($data));
	}
}
