<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\GlobalParams;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Helpers\Helper;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
		
		/* $path = base_path('public/1.txt');
		$csvFile = fopen($path, 'r');
		$arr = [];
		while(($line = fgets($csvFile)) !== FALSE){
			$line = explode("\t", $line);
			$line[1] = preg_replace('/\D/', '', $line[1]);
			$gol = trim($line[2]).' '.trim($line[3]);
			$pend = trim($line[4]);
			$pangk = trim($line[5]);
			$jabata = trim($line[6]);
			$jns_ket = trim($line[7]);
			$direk = trim($line[8]);
			$tmtc = trim($line[9]);
			$tmtg = trim($line[10]);
			
			
			// insert
			if(!empty($line[0])) $arr[] = $line;
			//dd($line);
			User::create([
				'name' => trim($line[0]),
				'nip' => preg_replace('/[^0-9]/', '', trim($line[1])),
				'password' => bcrypt('secret'),
				'json_user' => ['t2017' => 0, 't2018' => 12, 'total' => 12],
				'json_data' => ['golongan' => $gol, 'pendidikan' => $pend, 'pangkat' => $pangk, 'jabatan' => $jabata, 'jenis_ketenagaan' => $jns_ket, 'direktorat' => $direk, 'tmt_cpns' => $tmtc, 'tmt_golongan' => $tmtg]
			]);
			//
					
			$x = User::where('nip', preg_replace('/[^0-9]/', '', trim($line[1])))->firstOrFail();
			echo $x->nip.'<br/>';
			if($x){
				$y = ['json_data' => [
						'golongan' => $gol,
						'pendidikan' => $pend,
						'pangkat' => $pangk,
						'jabatan' => $jabata,
						'jenis_ketenagaan' => $jns_ket,
						'direktorat' => $direk,
						'tmt_cpns' => $tmtc,
						'tmt_golongan' => $tmtg
					]];
				//dd($x->json_data);
				if(isset($x->json_data['kd_dr'])){
					$y['json_data']['kd_dr'] = $x->json_data['kd_dr'];
				}
				//dd($y);
				$x->update($y);
			}			//dd(preg_replace('/[^0-9]/', '', trim($line[1])));196008051989012001
		}
		dd('ok'); */
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
		/* $user = User::where('nip', $data['nip'])->first();
		if(!$user){
		  return back()->withInput()->withErrors(['nip' => 'Email Already Exist']);
		} */
		
        return Validator::make($data, [
            'nip' => 'required|string|max:20|exists:users',
			'id_direktorat' => 'required|nullable',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function update(array $data)
    {
		$i1 = $i2 = $i3 = $i4 = null;
		if(isset($data['id_direktorat'])) $i1 = Helper::explodeHelper('-', $data['id_direktorat'], '0');
		if(isset($data['id_bagian'])) $i2 = Helper::explodeHelper('-', $data['id_bagian'], '0');
		if(isset($data['id_subagseksi'])) $i3 = Helper::explodeHelper('-', $data['id_subagseksi'], '0');
		if(isset($data['id_instalasi'])) $i4 = Helper::explodeHelper('-', $data['id_instalasi'], '0');
		
		$user = User::where([
					['nip', $data['nip']], 
					['active', '0']
				])->first();
				
		if($user == null){ //user not found
			return null;
		}
		
		$user->id_direktorat = $i1;
		$user->id_bagian = $i2;
		$user->id_subagseksi = $i3;
		$user->id_instalasi = $i4;
		$user->id_kastaff = Helper::returnExplodex();
		$user->password = bcrypt($data['password']);
		$user->active = true;
		$user->save();

        // assign user role
        $user->assignRole('User');

        return $user;
    }
}
