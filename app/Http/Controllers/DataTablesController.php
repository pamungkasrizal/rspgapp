<?php

namespace App\Http\Controllers;

use App\Authorizable;
use App\GlobalParams;
use App\Bagian;
use App\Subagseksi;
use App\Instalasi;
use App\Barang;
use App\Registrasi;
use App\Hais;
use App\Cuti;
use App\User;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use DB;

class DataTablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        if($request->ajax()){

			return true;
		}

		die(response()->json(['error' => 'Unauthenticated.'], 401));
	}
	
	public function getDataCuti(Request $request)
    {          
        $totalData = Cuti::where([['implementer->read', true], ['implementer->approve', true]])->with('user')->orderBy('created_at', 'desc')->count();
            
        $totalFiltered = $totalData; 

        $limit = $request->length;
        $start = $request->start;
        //$order = $columns[$request->order[0]['column']];
        $dir = $request->order[0]['dir'];
		
        if(empty($request->search['value'])){     
		
            $data = Cuti::with('user')
						 ->where([['implementer->read', true], ['implementer->approve', true]])
						 ->offset($start)
                         ->limit($limit)
                         ->orderBy('created_at', 'desc'/* $order, $dir */)
                         ->get();
        }else {
			
            $search = $request->search['value'];
			
            $data =  Cuti::whereHas('user', function ($query) use($search) {
								$query->where('name', 'LIKE', '%'.$search.'%');
							})
							->with('user')
							->where([['implementer->read', true], ['implementer->approve', true]])
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('created_at', 'desc'/* $order,$dir */)
                            ->get();

            $totalFiltered = Cuti::whereHas('user', function ($query) use($search) {
								$query->where('name', 'LIKE', '%'.$search.'%');
							})
							->with('user')
							->where([['implementer->read', true], ['implementer->approve', true]])
                            ->count();
        }
          
        $json_data = [
						"draw"            => intval($request->draw),  
						"recordsTotal"    => intval($totalData),  
						"recordsFiltered" => intval($totalFiltered), 
						"data"            => $data   
                    ];
            
		die(json_encode($json_data));
        
    }
	
	public function getReportHais(Request $request)
    {
        $totalData = Hais::orderBy('created_at', 'desc')->count();
            
        $totalFiltered = $totalData; 

        $limit = $request->length;
        $start = $request->start;
        //$order = $columns[$request->order[0]['column']];
        $dir = $request->order[0]['dir'];
		
        if(empty($request->search['value'])){     
		
            $data = Hais::offset($start)
                         ->limit($limit)
                         ->orderBy('created_at', 'desc'/* $order, $dir */)
                         ->get();
        }else {
			
            $search = strtoupper($request->search['value']);
			
            $data =  Hais::where('no_reg', 'LIKE', '%'.$search.'%')
							->orWhere('data_kunjungan->nama_pas', 'LIKE', '%'.$search.'%')
							->orWhere('data_kunjungan->no_mr', 'LIKE', '%'.$search.'%')
							->orWhere('data_kunjungan->ds_dep', 'LIKE', '%'.$search.'%')
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('created_at', 'desc'/* $order,$dir */)
                            ->get();

            $totalFiltered = Hais::where('no_reg', 'LIKE', '%'.$search.'%')
								->orWhere('data_kunjungan->nama_pas', 'LIKE', '%'.$search.'%')
								->orWhere('data_kunjungan->no_mr', 'LIKE', '%'.$search.'%')
								->orWhere('data_kunjungan->ds_dep', 'LIKE', '%'.$search.'%')
								->count();
        }
          
        $json_data = [
						"draw"            => intval($request->draw),  
						"recordsTotal"    => intval($totalData),  
						"recordsFiltered" => intval($totalFiltered), 
						"data"            => $data   
                    ];
            
		die(json_encode($json_data));
        
    }
	
	public function getDataUser(Request $request)
    {
        $totalData = User::orderBy('name', 'asc')->count();
            
        $totalFiltered = $totalData; 

        $limit = $request->length;
        $start = $request->start;
        //$order = $columns[$request->order[0]['column']];
        $dir = $request->order[0]['dir'];
		
        if(empty($request->search['value'])){     
		
            $data = User::offset($start)
                         ->limit($limit)
                         ->orderBy('name', 'asc')
                         ->get();
        }else {
			
            $search = $request->search['value'];
			
            $data =  User::where('name', 'LIKE', '%'.$search.'%')
							->orWhere('nip', 'LIKE', '%'.$search.'%')
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy('name', 'asc')
                            ->get();

            $totalFiltered = User::where('name', 'LIKE', '%'.$search.'%')
								->orWhere('nip', 'LIKE', '%'.$search.'%')
								->count();
        }
          
        $json_data = [
						"draw"            => intval($request->draw),  
						"recordsTotal"    => intval($totalData),  
						"recordsFiltered" => intval($totalFiltered), 
						"data"            => $data   
                    ];
            
		die(json_encode($json_data));
        
    }
}
