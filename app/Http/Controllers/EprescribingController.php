<?php

namespace App\Http\Controllers;

use App\Eprescribing;
use App\Dokter;
use App\Registrasi;
use App\Barang;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class EprescribingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$conditions[] = ['closed', 'N'];
		$conditions[] = ['inap', 'N'];
		if(Auth::user()->json_data != null) $conditions[] = ['kd_dr', Auth::user()->json_data['kd_dr']];
		
		$data = Registrasi::select('no_mr', 'no_reg', 'nama_pas', 'umur', 'ds_dep', 'ds_dr', 'tgl_reg')
					->where($conditions)
					->whereDate('tgl_reg', date('Y-m-d'))
					->orderBy('tgl_reg', 'asc')
					->limit(10)
					->get();
					
        return view('eprescribing.index', compact('data'));
    }
	
	public function history($no_mr, $no_reg = null)
    {
		$data = Eprescribing::select(DB::raw('DISTINCT(no_order)'), 'tgl_order', 'ds_dep', 'ds_dr', 'no_reg')
					->where('no_mr', $no_mr)
					->get();
					
		$dataPasien = Registrasi::select('no_mr','nama_pas','umur','lp')
					->where('no_mr', $no_mr)
					->orderBy('tgl_reg', 'desc')
					->limit(1)
					->first();
		
        return view('eprescribing.history', compact('data','dataPasien','no_reg'));
    }
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($no_reg, $no_order = null)
    {
		$dataEpresc = null;
		$data = Registrasi::select('no_mr','no_reg','nama_pas','umur','kd_dep','ds_dep','kd_dr','ds_dr','lp','tinggi_badan','berat_badan','ds_icd_masuk','kd_perush')
					->where('no_reg', $no_reg)
					->first();
					
		$jamMakan = DB::connection('apotikFarmasi')->table('m_jam_makan')->orderBy('nama_jam_makan', 'asc')->pluck('nama_jam_makan', 'kd_jam_makan');
		$lokasiPg = DB::connection('apotikFarmasi')->table('m_ket_etiket')->orderBy('nama_ket_etiket', 'asc')->pluck('nama_ket_etiket', 'kd_ket');
		$satuan = DB::connection('apotikFarmasi')->table('m_satuan_etiket')->orderBy('nama_satuan_etiket', 'asc')->pluck('nama_satuan_etiket', 'kd_satuan_etiket');
		
		if($no_order != null){
			$dataEpresc = Eprescribing::where('no_order', $no_order)->get();
		}
		
        return view('eprescribing.order', compact('data','jamMakan', 'lokasiPg', 'satuan', 'dataEpresc', 'no_reg'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $no_reg)
    {
		if ( Eprescribing::insert($request->rows) ) {

            flash('Resep has been created.');

        } else {
            flash()->error('Unable to create Resep.');
        }
		
        return redirect()->route('historyEprescribing', $request->no_mr);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Escrubbing  $escrubbing
     * @return \Illuminate\Http\Response
     */
    public function show(Escrubbing $escrubbing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Escrubbing  $escrubbing
     * @return \Illuminate\Http\Response
     */
    public function edit(Escrubbing $escrubbing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Escrubbing  $escrubbing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Escrubbing $escrubbing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Escrubbing  $escrubbing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Escrubbing $escrubbing)
    {
        //
    }
}
