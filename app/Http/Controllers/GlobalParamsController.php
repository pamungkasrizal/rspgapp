<?php

namespace App\Http\Controllers;

use App\GlobalParams;
use Illuminate\Http\Request;

class GlobalParamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GlobalParams  $globalParams
     * @return \Illuminate\Http\Response
     */
    public function show(GlobalParams $globalParams)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GlobalParams  $globalParams
     * @return \Illuminate\Http\Response
     */
    public function edit(GlobalParams $globalParams)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GlobalParams  $globalParams
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GlobalParams $globalParams)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GlobalParams  $globalParams
     * @return \Illuminate\Http\Response
     */
    public function destroy(GlobalParams $globalParams)
    {
        //
    }
}
