<?php

namespace App\Http\Controllers;

use App\Hais;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HaisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hais.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'txt_search' => 'required|min:1',
            'data_kunjungan.no_reg' => 'required|min:1',
            'data_kunjungan.no_mr' => 'required|min:1'
        ]);
		unset($request['_token']);
		unset($request['txt_search']);
		if(!isset($request->data_irs)) $request->merge(['data_irs' => null]);
		$request->merge(['data_user' => ['id_user' => Auth::user()->id, 'name' => Auth::user()->name, 'note' => '', 'nip' => Auth::user()->nip]]);
		
		if ( Hais::updateOrCreate(['no_reg' => $request->no_reg], $request->all()) ) {

            flash('Surveilans HAIs has been created.');

        } else {
            flash()->error('Unable to create Surveilans HAIs.');
        }
		
        return redirect()->route('hais.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hais  $hais
     * @return \Illuminate\Http\Response
     */
    public function show(Hais $hais)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hais  $hais
     * @return \Illuminate\Http\Response
     */
    public function edit(Hais $hais)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hais  $hais
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hais $hais)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hais  $hais
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hais $hais)
    {
        //
    }
	
	public function reportHais()
    {
		/* $arr = [];
		
		$data = Hais::select('data_kunjungan->ds_dep as ruangan', 'data_medis', 'data_pemasangan', 'data_irs')
					->whereYear('created_at', date('Y'))
					->orderBy('created_at', 'desc')
					->get();
		foreach($data as $keys){
			$arr[] = $keys->ruangan;
			foreach($keys->data_pemasangan as $key1){
				$arr[][$keys->ruangan] = $arr[$keys->ruangan][$key1] + $arr[$keys->ruangan][$key1]['day'];
			}
		}
		
		dd($arr->toArray()); */
        return view('hais.report');
    }
}
