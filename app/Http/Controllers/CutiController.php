<?php

namespace App\Http\Controllers;

use App\Cuti;
use App\User;
use App\GlobalParams;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Crypt;
use PDF;
//\DB::connection()->enableQueryLog();

class CutiController extends Controller
{
    /**\DB::connection()->enableQueryLog(); \DB::getQueryLog();
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */	
	 
    public function index()
    {		
		$head = $kepeg = null;
		$implementer = Helper::queryCuti([['cutis.implementer->id_user', Auth::user()->id],['cutis.implementer->approve', false]]);
							
		if(Auth::user()->id_kastaff == 1) 
			$head = Helper::queryCuti([['cutis.head->id_user', Auth::user()->id],['cutis.head->approve', false],['cutis.implementer->approve', true],['cutis.head->note', '']]);

		$direksi = $this->direksi();
		if(Auth::user()->hasRole('Admin') || $direksi->status == true)
			$kepeg = Helper::queryCuti([['cutis.implementer->approve', true], ['cutis.head->approve', true], ['cutis.json_data->approve', false], ['cutis.json_data->id_user', Auth::user()->id]]);
							
		$history = Cuti::where('id_user', Auth::user()->id)->with('user')->orderBy('created_at', 'desc')->limit(10)->get();
		
        return view('cuti.index', compact('history', 'implementer', 'head', 'kepeg', 'direksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	
    public function create()
    {	
		$totalCuti = Auth::user()->json_user['total'];
		$tglmerah = $this->tglmerah();
		$cutiType = GlobalParams::where('xcond', 'jeniscuti')->orderBy('description', 'asc')->pluck('description', 'sq_id');
		$user = Auth::user();
		$implementer = $this->implementer($user);
		$head = $this->head($user);
		
        return view('cuti.new', compact('cutiType', 'implementer', 'head', 'tglmerah', 'totalCuti'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$max = ($request->type == 5) ? Auth::user()->json_user['total'] : 999;
        $this->validate($request, [
            'type' => 'required|nullable',
            'qty' => 'numeric|min:1|max:'.$max.'|nullable',
            'reason' => 'required|min:5',
			'implementer' => 'required|nullable',
			'head' => 'required|nullable'
        ]);
		
		$start = Helper::explodeHelper(' - ', $request->date, '0');
		$end = Helper::explodeHelper(' - ', $request->date, '1');
		$request->merge(['implementer' => ['id_user' => $request->implementer, 'read' => false, 'approve' => false, 'note' => '', 'date' => '']]);
		$request->merge(['head' => ['id_user' => $request->head, 'read' => false, 'approve' => false, 'note' => '', 'note' => '', 'date' => '']]);
		$request->merge(['json_data' => ['id_user' => $this->direksi()->id, 'read' => false, 'approve' => false, 'note' => '', 'note' => '', 'date' => '']]);
		$request->merge(['start_date' => date('Y-m-d', strtotime($start))]);
		$request->merge(['end_date' => date('Y-m-d', strtotime($end))]);
		$request->merge(['comp_name' => gethostname()]);
		$request->merge(['ip_address' => \Request::ip()]);

		if ( $cuti = Cuti::create($request->all()) ) {

            flash('Cuti has been created.');

        } else {
            flash()->error('Unable to create cuti.');
        }
        return redirect()->route('cutis.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function view(Cuti $cuti)
    {
		$implementer = Cuti::where([['id', $cuti->id], ['implementer->id_user', Auth::user()->id]])->first();
		$head = Cuti::where([['id', $cuti->id], ['head->id_user', Auth::user()->id]])->first();
		$json_data = Cuti::where([['id', $cuti->id], ['json_data->id_user', Auth::user()->id]])->first();

		$updateRead = Cuti::find($cuti->id);
		if($implementer !== null) $updateRead['implementer->read'] = true;
		if($head !== null) $updateRead['head->read'] = true;
		if($json_data !== null) $updateRead['json_data->read'] = true;
		$updateRead->save();
		
		$datacuti = Helper::queryCuti([['cutis.id', $cuti->id]]);
		$datacuti = $datacuti[0];
		
		$imp = Helper::getFullUserData($datacuti->implementer['id_user']);
		
		$hea = Helper::getFullUserData($datacuti->head['id_user']);
		$dir = Helper::getFullUserData($datacuti->json_data['id_user']);
		$cutiType = GlobalParams::where([['xcond', 'jeniscuti'],['sq_id', $datacuti->type]])->select('description')->first();
		$direksi = $this->direksi();
		
        return view('cuti.view', compact('datacuti', 'implementer', 'head', 'imp', 'hea', 'dir', 'cutiType', 'direksi'));
    }
	
	public function status(Request $request)
    {
		$this->validate($request, [
            'note' => 'required|min:1'
        ]);
		
		$updateApproved = Cuti::find($request->id_cuti);
		$updateApproved[$request->field.'->approve'] = ($request->rea == 0) ? false : true;
		$updateApproved[$request->field.'->note'] = 'note : '.$request->note;
		$updateApproved[$request->field.'->date'] = date('Y-m-d H:i:s');
		$updateApproved->save();
		
		if($request->rea == 1 && $request->field == 'json_data' && $updateApproved->type == 5){ //memotong sisa cuti
			$year = date('Y');
			$user = User::find($updateApproved->id_user);
			$user['json_user->t'.$year] = $user->json_user['t'.$year] - $updateApproved->qty;
			$user['json_user->total'] = $user->json_user['total'] - $updateApproved->qty;
			$user->save();
		}
		
		flash()->success('Cuti has been updated.');
		return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function edit(Cuti $cuti)
    {
		if(Auth::user()->id !== $cuti->id_user) return redirect()->back();
		if(!empty($cuti->implementer['note'])){
			return redirect()->route('view/cuti', $cuti->id);
		}
		
		$cuti->date = date('m/d/Y', strtotime($cuti->start_date)).' - '.date('m/d/Y', strtotime($cuti->end_date));

		$totalCuti = Auth::user()->json_user['total'];		
		$tglmerah = $this->tglmerah();
		$cutiType = GlobalParams::where('xcond', 'jeniscuti')->orderBy('description', 'asc')->pluck('description', 'sq_id');
		$user = Auth::user();
		$implementer = $this->implementer($user);
		$head = $this->head($user);
		
        return view('cuti.edit', compact('cuti', 'cutiType', 'implementer', 'head', 'tglmerah', 'totalCuti'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cuti $cuti)
    {
        $this->validate($request, [
            'type' => 'required|nullable',
            'qty' => 'numeric|min:3|max:'.Auth::user()->json_user['total'].'|nullable',
            'reason' => 'required|min:20',
			'implementer' => 'required|nullable',
			'head' => 'required|nullable'
        ]);
		
		$cuti = Cuti::findOrFail($cuti->id);
		
		$start = Helper::explodeHelper(' - ', $request->date, '0');
		$end = Helper::explodeHelper(' - ', $request->date, '1');
		$request->merge(['implementer' => ['id_user' => $request->implementer, 'read' => false, 'approve' => false, 'note' => '']]);
		$request->merge(['head' => ['id_user' => $request->head, 'read' => false, 'approve' => false, 'note' => '']]);
		$request->merge(['start_date' => date('Y-m-d', strtotime($start))]);
		$request->merge(['end_date' => date('Y-m-d', strtotime($end))]);
		$request->merge(['comp_name' => gethostname()]);
		$request->merge(['ip_address' => \Request::ip()]);
		
		$cuti->update($request->all());

        flash()->success('Cuti has been updated.');

        return redirect()->route('cutis.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cuti $cuti)
    {
        //
    }
	
	public function updateCuti()
	{
		$pnsJmlCuti = GlobalParams::where([['xcond', 'jmlcuti'], ['sq_id', 1]])->pluck('description')->first();
		$nonJmlCuti = GlobalParams::where([['xcond', 'jmlcuti'], ['sq_id', 2]])->pluck('description')->first();

		$user = User::where([['json_user', '!=', null], ['active', 1]])->select('id', 'nip','json_user','name')->get();
		
		if((date('m-d') == '01-01')){ //autoUpdate
			foreach($user as $val){
				$prevYear = date('Y')-3;
				$newYear = date('Y');
				$tahun = json_encode($val->json_user, true);
				$tahun = json_decode($tahun, true);

				if(strlen($val['nip']) > 15){ //PNS
					if($tahun['total'] == 12 || $tahun['total'] == 18){
						$sisa = floor($tahun['t'.$prevYear]/2);
						$tahun['t'.$newYear] = $pnsJmlCuti;
						$tahun['total'] = $pnsJmlCuti + $sisa;
					}else{
						$tahun['t'.$newYear] = $pnsJmlCuti;
						$tahun['total'] = $pnsJmlCuti;
					}
				}else{ //NON PNS
					$tahun['t'.$newYear] = $nonJmlCuti;
					$tahun['total'] = $nonJmlCuti;
				}
				
				$userUpdate = User::where('id', $val->id)->first();
				$userUpdate->json_user = $tahun;
				$userUpdate->save();
			}
			die('Update Done!!');
		}
		die('Update Failed!!');
	}
	
	private function implementer($user)
	{
		$conditions = $orWhere = [];
		$conditions[] = ['id_instalasi', $user->id_instalasi];
		$conditions[] = ['id_subagseksi', $user->id_subagseksi];
		$conditions[] = ['id_bagian', $user->id_bagian];
		$conditions[] = ['id_direktorat', $user->id_direktorat];
		$conditions[] = ['id_kastaff', $user->id_kastaff];
		
		if($user->id_instalasi !== null && $user->id_kastaff == 1){ //ka instalasi
			unset($conditions[4]);
			$orWhere[]    = ['id_bagian', $user->id_bagian];
			$orWhere[]    = ['id_instalasi', '!=', null];
			$orWhere[]    = ['id_kastaff', 1];
		}
		
		if($user->id_subagseksi !== null && $user->id_instalasi == null && $user->id_kastaff == 1){ //ka subagseksi
			unset($conditions[4]);
			$orWhere[]    = ['id_bagian', $user->id_bagian];
			$orWhere[]    = ['id_subagseksi', '!=', null];
			$orWhere[]    = ['id_instalasi', null];
			$orWhere[]    = ['id_kastaff', 1];
		}
		
		if($user->id_bagian !== null && $user->id_subagseksi == null && $user->id_kastaff == 1){ //ka subagseksi
			$orWhere[]    = ['id_direktorat', $user->id_direktorat];
			$orWhere[]    = ['id_bagian', '!=', null];
			$orWhere[]    = ['id_instalasi', null];
			$orWhere[]    = ['id_kastaff', 1];
		}
		
		if($user->id_direktorat !== null && $user->id_bagian == null && $user->id_kastaff == 1){ //direksi
			$x = ($user->id_direktorat == 1) ? 2 : 1;
			$orWhere[]    = ['id_direktorat', $x];
			$orWhere[]    = ['id_bagian', null];
			$orWhere[]    = ['id_kastaff', 1];
			
			$conditions[2] = ['id_bagian', '!=', null];
		}
		
		$implementer = User::where($conditions)
						->orWhere($orWhere)
						->where([['id', '!=', $user->id], ['active', 1]])
						->orderBy('name', 'asc')
						->pluck('name', 'id');
		if(in_array($user->id_direktorat, [3,4])) $implementer = User::where('active', 1)->whereIn('id_direktorat', [3,4])->orderBy('name', 'asc')->pluck('name', 'id');
		if(in_array($user->id_direktorat, [5,6,7])) $implementer = User::where('active', 1)->whereIn('id_direktorat', [5,6,7])->orderBy('name', 'asc')->pluck('name', 'id');
			
		unset($implementer[$user->id]);
		
		return $implementer;
	}
	
	private function head($user)
	{	
		$conditions[] = ['id_instalasi', $user->id_instalasi];
		$conditions[] = ['id_subagseksi', $user->id_subagseksi];
		$conditions[] = ['id_bagian', $user->id_bagian];
		$conditions[] = ['id_direktorat', $user->id_direktorat];

		if($user->id_instalasi !== null && $user->id_kastaff == 1){
			$conditions[0] = ['id_instalasi', null];
			$conditions[1] = ['id_subagseksi', null];
			$conditions[2] = ['id_bagian', null];
		}
		if($user->id_subagseksi !== null && $user->id_instalasi == null && $user->id_kastaff == 1)
			$conditions[1] = ['id_subagseksi', null];
		if($user->id_bagian !== null && $user->id_subagseksi == null && $user->id_kastaff == 1)
			$conditions[2] = ['id_bagian', null];
		if($user->id_direktorat !== null && $user->id_bagian == null && $user->id_kastaff == 1)
			$conditions[3] = ['id_direktorat', null];

		$head = User::where($conditions)
						->where([['id_kastaff', 1], ['active', 1], ['id', '!=', $user->id]])
						->orderBy('name', 'asc')
						->pluck('name', 'id');
		return $head;
	}
	
	private function tglmerah()
	{
		$year = date('Y');
		$tglmerah = GlobalParams::whereIn('description', [$year, $year+1])
								->where('xcond', 'tglmerah')
								->pluck('json_data')
								->toArray();
		$tglmerah = json_encode(array_pluck($tglmerah[0], 'tgl'));
		
		return $tglmerah;
	}
	
	private function direksi()
	{
		$id_direktorat = (Auth::user()->id_bagian == NULL) ? NULL : Auth::user()->id_direktorat;
		$direksi = User::where([['id_direktorat', $id_direktorat], ['id_bagian', NULL], ['id_kastaff', 1]])->select('id', 'nip', 'name')->first();
		$direksi['status'] = ($direksi !== null) ? true : false;

		return $direksi;
	}
	
	public function letter(Cuti $cuti)
	{
		$user = Helper::getFullUserData($cuti->id_user);
		$direk = Helper::getFullUserData($cuti->json_data['id_user']);

		PDF::SetTitle('Surat Izin Cuti Tahunan');
		PDF::SetAutoPageBreak(true, 0);

		Helper::tcpdfHeaderHelper('P','F4');
		
		$html = '<div style="border-bottom-style: dashed;"> <div style="text-align:center"><u>SURAT IZIN CUTI TAHUNAN</u></div> <span style="text-align:center">Nomor : KP.04.02./2/..../'.date('Y').'</span> <br/> <div>Diberikan Cuti Tahunan untuk Tahun '.date('Y', strtotime($cuti->created_at)).' kepada Pegawai :</div> <br/> <table width="100%"> <tr> <td width="22%">Nama</td> <td width="1%">:</td> <td width="72%">'.$user->name.'</td> </tr> <tr> <td>NIP/NPP</td> <td>:</td> <td>'.$user->nip.'</td> </tr> <tr> <td>Pangkat Gol. Ruang</td> <td>:</td> <td>'.$user->json_data['pangkat'].' - '.$user->json_data['golongan'].'</td> </tr> <tr> <td>Jabatan</td> <td>:</td> <td>'.$user->json_data['jabatan'].'</td> </tr> </table> <br/> <div>Selama '.$cuti->qty.' hari kerja terhitung mulai tanggal '.date('d F Y', strtotime($cuti->start_date)).' sampai '.date('d F Y', strtotime($cuti->end_date)).'</div> <span>Dengan ketentuan sebagai berikut :</span> <div>a. Sebelum menjalankan cuti tahunan wajib melaporkan diri kepada atasan langsung.</div> <span>b. Setelah menjalankan cuti tahunan wajib melaporkan diri kepada atasan langsung dan bekerja kembali sebagaimana mestinya.</span> <br/> <table width="100%"> <tr> <td width="51%"></td> <td width="4%"></td> <td width="45%">'.date('d F Y', strtotime($cuti->created_at)).'</td> </tr> <tr> <td></td> <td>a.n.</td> <td>Direktur Utama</td> </tr> <tr> <td></td> <td></td> <td>'.($direk->jabatan == 'Direktur Utama' ? '' : 'Direktur Keuangan Dan Administrasi Umum').'</td> </tr><tr> <td height="17.3"></td> <td></td> <td><br/></td> </tr><tr> <td></td> <td></td> <td><b>Evi Nursafinah, SE, MPH</b></td> </tr><tr> <td></td> <td></td> <td>NIP : 197406112000032002</td> </tr></table> </div>';

		PDF::SetFont('helvetica', '', 12);
		PDF::writeHTML($html, true, 0, true, true);
		$style = [
			'border' => false,
			'padding' => 0,
			'fgcolor' => [0,0,0],
			'bgcolor' => false
		];
		PDF::write2DBarcode('http://10.16.0.150:8000/cutis/'.$cuti->id.'/approval', 'QRCODE,H', 160, 70, 25, 25, $style, 'N');
		PDF::Ln(72);

		Helper::tcpdfHeaderTextHelper();
		PDF::Ln(0);
		PDF::SetFont('helvetica', '', 12);
		PDF::writeHTML($html, true, 0, true, true);
		$style = [
			'border' => false,
			'padding' => 0,
			'fgcolor' => [0,0,0],
			'bgcolor' => false
		];
		PDF::write2DBarcode('http://10.16.0.150:8000/cutis/'.$cuti->id.'/approval', 'QRCODE,H', 160, 235, 25, 25, $style, 'N');
		
		PDF::Output('cuti-tahunan-'.$cuti->name.'.pdf');
	}
	
	public function approval($id)
	{
		$data = Helper::queryCuti([['cutis.id', $id],['cutis.json_data->approve', true]]);
		$data = $data[0];
		
		return view('cuti.approval', compact('data'));
	}
	
	public function reportCuti()
	{
		if(Auth::user()->hasRole('Admin') || Auth::user()->id_bagian == NULL || Auth::user()->id == '3f920fe0-7674-42cb-bd4f-e937260f8797')
		{
			return view('cuti.report');
		}else{
			return redirect()->route('cutis.index');
		}
	}
}
