<?php

namespace App\Http\Controllers;

use App\Subagseksi;
use Illuminate\Http\Request;

class SubagseksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subagseksi  $subagseksi
     * @return \Illuminate\Http\Response
     */
    public function show(Subagseksi $subagseksi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subagseksi  $subagseksi
     * @return \Illuminate\Http\Response
     */
    public function edit(Subagseksi $subagseksi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subagseksi  $subagseksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subagseksi $subagseksi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subagseksi  $subagseksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subagseksi $subagseksi)
    {
        //
    }
}
