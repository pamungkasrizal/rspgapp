<?php

namespace App\Http\Controllers;

use App\Instalasi;
use Illuminate\Http\Request;

class InstalasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Instalasi  $instalasi
     * @return \Illuminate\Http\Response
     */
    public function show(Instalasi $instalasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Instalasi  $instalasi
     * @return \Illuminate\Http\Response
     */
    public function edit(Instalasi $instalasi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Instalasi  $instalasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Instalasi $instalasi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Instalasi  $instalasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instalasi $instalasi)
    {
        //
    }
}
