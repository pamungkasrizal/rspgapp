<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instalasi extends Model
{
    public function subagseksi()
    {
        return $this->belongsTo('App\Subagseksi', 'id_subagseksi', 'id');
    }
}
