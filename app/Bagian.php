<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bagian extends Model
{
    public function direktorat()
    {
        $data = $this->belongsTo('App\GlobalParams','id_direktorat', 'sq_id');
		$data->getQuery()->where('xcond', 'direktorat');
		return $data;
    }
}
