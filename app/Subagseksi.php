<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subagseksi extends Model
{	
    public function bagian()
    {
        return $this->belongsTo('App\Bagian', 'id_bagian', 'id');
    }
}
