<?php

namespace App\Helpers;
use App\User;
use App\Cuti;
use App\GlobalParams;
use PDF;

class Helper {
	
	public static $returnExplode = 0;
	
	//Register
	public static function selectboxOption($dataArray, $default, $kepala = null, $staff = null, $multi = false, $search = null, $replace = null)
	{
		$htm1 = $htm2 = '';
		$htm1 .= '<optgroup label="Struktural">';
		$htm2 .= '<optgroup label="Jabatan">';
		
		foreach ($dataArray as $key => $val){
			$new = ($kepala) ? $kepala.' '.$val : str_replace_first($search, $replace, $val);
			
			if($search == 'Direktorat'){
				if(!in_array($key, array(3,4,5,6,7))) $htm1 .= '<option value="'. $key .'"> '. $val .' </option>';
			}else{
				 $htm1 .= '<option value="'. $key .'"> '. $val .' </option>';
			}
			$htm2 .= '<option value="'. $key .'-1"> '. $new .' </option>';
			$htm2 .= ($staff) ? '<option value="'. $key .'-2"> '. $staff.' '. $val .' </option>' : '';
		}
		
		$htm1 .= '</optgroup>';
		$htm2 .= '</optgroup>';
		
		if($multi) $htm2 = static::selectboxOption2($dataArray, null, $kepala, $staff);
		
		$default = '<option value="" selected="" disabled="">'. $default .'</option>';

		return $default.$htm1.$htm2;
	}
	
	public static function selectboxOption2($dataArray, $default = null, $kepala = null, $staff = null, $search = null, $replace = null)
	{
		$htm1 = '';
		
		foreach ($dataArray as $key => $val){
			$new = ($kepala) ? $kepala.' '.$val : str_replace_first($search, $replace, $val);
			/* if(ends_with($val, 'Komite Medik') == true) $new = $val;//komite medik
			if(ends_with($val, 'Komite Medik') == true) $staff = null;//komite medik */
			
			$htm1 .= '<optgroup label="'. $val .'">';
			$htm1 .= '<option value="'. $key .'-1"> '. $new .' </option>';
			$htm1 .= ($staff) ? '<option value="'. $key .'-2"> '. $staff.' '. $val .' </option>' : '';
			$htm1 .= '</optgroup>';
		}
		
		if($default) $default = '<option value="" selected="" disabled="">'. $default .'</option>';

		return $default.$htm1;
	}
	
	public static function explodeHelper($parameter, $subject, $index = null)
	{
		if(substr_count($subject, $parameter) < 1) return $subject;
		
		$ex = explode($parameter, $subject);
		if($index != null){
			static::$returnExplode = $ex[1];
			return $ex[$index];
		}else{
			return $ex;
		}
	}
	
	public static function returnExplodex()
	{
		return static::$returnExplode;
	}
	
	public static function getFullUserData($id_user)
	{
		$data = User::where('id', $id_user)->with('direktorat','bagian','subagseksi','instalasi')->first();
		$jabatan = null;
		$jabatan = ($data->direktorat !== null) ? str_replace('Direktorat', 'Direktur', $data->direktorat->description) : 'Direktur Utama';
		$jabatan = ($data->bagian !== null) ? 'Kepala '.$data->bagian->name : $jabatan;
		$jabatan = ($data->subagseksi !== null) ? 'Kepala '.$data->subagseksi->name : $jabatan;
		$jabatan = ($data->instalasi !== null) ? 'Kepala '.$data->instalasi->name : $jabatan;
		$jabatan = in_array($data->id_direktorat, [3,4,5,6,7]) ? GlobalParams::where([['xcond', 'direktorat'],['sq_id', $data->id_direktorat]])->select('description')->first()->description : $jabatan;
		
		$data['jabatan'] = ($data->id_kastaff == 2) ? str_replace('Kepala', 'Staff', $jabatan) : $jabatan;
		
		return $data;
	}
	
	public static function tcpdfHeaderHelper($p, $h)
	{
		PDF::setHeaderCallback(function($pdf){
			//logo kemenkes
			$image_file = public_path().'/assets/images/logo-kemenkes.png';			
			$pdf->Image($image_file, 10.1, 7, 25, 25, '', '', '', false, 300, 'L');
			//logo rspg
			$image_file = public_path().'/assets/images/logo-rspg.png';
			$pdf->Image($image_file, 8, 12, 36, 19, '', '', '', false, 300, 'R');
			//header text
			$pdf->SetFont('centuryschoolbookb', 'B', 18);
			$pdf->setFontSpacing(0.254);
			$pdf->Cell(0, 16, 'KEMENTERIAN KESEHATAN RI', 0, false, 'C', 0, '', 0, true, 'T', 'M');
			$pdf->setFontSpacing(0);
			$pdf->Ln(11.5);
			
			$pdf->SetFont('arialnarrow', '', 14);
			$pdf->Cell(0, 0, 'DIREKTORAT JENDRAL PELAYANAN KESEHATAN', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$pdf->Ln();
			$pdf->Cell(0, 0, 'RUMAH SAKIT PARU Dr. M. GOENAWAN PARTOWIDIGDO', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$pdf->Ln();
			
			$pdf->SetFont('calibri', '', 12);
			$pdf->Cell(0, 0, 'Jalan Raya Puncak KM 83, Kotak Pos 28 Cisarua Bogor 16750', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$pdf->Ln();
			$pdf->Cell(0, 0, 'Telp. (0251) 8253630, 8257663.  Faksimile (0251) 8254782, 8257662', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$pdf->Ln();
			$pdf->Cell(0, 0, 'Website : www.rspg-cisarua.co.id, Surat Elektronik : info@rspg-cisarua.co.id', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$pdf->Ln(7);
			$top_border = [
			   'T' => ['width' => 1, 'color' => [0,0,0], 'dash' => 0, 'cap' => 'square']
			];
			$pdf->Cell(0, 0, '', $top_border, false, 'C', 0, '', 0, false, 'T', 'M');			
		});
		
		PDF::AddPage($p, $h);
		PDF::Ln(32);
	}
	
	public static function tcpdfHeaderTextHelper()
	{
		$image_file = public_path().'/assets/images/logo-kemenkes.png';			
		PDF::Image($image_file, 10.1, 177, 25, 25, '', '', '', false, 300, 'L');
		//logo rspg
		$image_file = public_path().'/assets/images/logo-rspg.png';
		PDF::Image($image_file, 8, 182, 36, 19, '', '', '', false, 300, 'R');
		PDF::SetFont('centuryschoolbookb', 'B', 18);
		PDF::setFontSpacing(0.254);
		PDF::Cell(0, 16, 'KEMENTERIAN KESEHATAN RI', 0, false, 'C', 0, '', 0, true, 'T', 'M');
		PDF::setFontSpacing(0);
		PDF::Ln(11.5);
		
		PDF::SetFont('arialnarrow', '', 14);
		PDF::Cell(0, 0, 'DIREKTORAT JENDRAL PELAYANAN KESEHATAN', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		PDF::Ln();
		PDF::Cell(0, 0, 'RUMAH SAKIT PARU Dr. M. GOENAWAN PARTOWIDIGDO', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		PDF::Ln();
		
		PDF::SetFont('calibri', '', 12);
		PDF::Cell(0, 0, 'Jalan Raya Puncak KM 83, Kotak Pos 28 Cisarua Bogor 16750', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		PDF::Ln();
		PDF::Cell(0, 0, 'Telp. (0251) 8253630, 8257663.  Faksimile (0251) 8254782, 8257662', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		PDF::Ln();
		PDF::Cell(0, 0, 'Website : www.rspg-cisarua.co.id, Surat Elektronik : info@rspg-cisarua.co.id', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		PDF::Ln(7);
		$top_border = [
		   'T' => ['width' => 1, 'color' => [0,0,0], 'dash' => 0, 'cap' => 'square']
		];
		PDF::Cell(0, 0, '', $top_border, false, 'C', 0, '', 0, false, 'T', 'M');
	}
	
	public static function queryCuti($where)
	{
		$data = Cuti::where($where)
						->with('user')
						->orderBy('created_at', 'desc')
						->limit(10)->orderBy('cutis.created_at', 'desc')
						->get();
		return $data;
	}
	
}