//var holiday = $tgllibur;

$(document).ready(function(){
	//cuti add/update ===============================
	var shift = 0;  //['0' => 'non shift', '1' => 'Pola Shift']
	
	$('input[name=shift]').on('ifChecked', function(event){
		shift = $(this).val(); // alert value
		
		$('span.cutiHari').html('0');
		$('#qty').val(0);
		
		dateRefresh(shift, null, null);
	});
	
	$('select[name=type]').on('change', function(){
		var $val = $(this).val();
		disabledEnableCuti($val, 1)
	});
	
	$value = $('select[name=type]').val();
	disabledEnableCuti($value, 1);
	dateRefresh(shift, 1, null);
	$('span.cutiHari').html($('#qty').val());
	
	$("#datatable-rspgapp").DataTable({
		dom: "Bfrtip",
		buttons: [
			{
				extend: "copy",
				className: "btn-sm"
			},
			{
				extend: "csv",
				className: "btn-sm"
			},
			{
				extend: "excel",
				className: "btn-sm"
			},
			{
				extend: "pdfHtml5",
				className: "btn-sm"
			},
			{
				extend: "print",
				className: "btn-sm"
			},
		],
		responsive: true
	});
});

function dateRefresh(shift, check, typeCuti){
	
	if(typeof $('#reservation').val() === "undefined") return false;
	
	var $checkVal = $('#reservation').val(),
		$checkVal = $checkVal.split(' - ');
		$startDate = (check !== null) ? $checkVal[0] : moment().startOf("day"),
		$endDate = (check !== null) ? $checkVal[1] : moment().startOf("day"),
		$minDate = (typeCuti !== null && typeCuti == 4) ? moment().subtract(1,'months').startOf('month') : $startDate;
		
	$('#reservation').daterangepicker({
		startDate: $startDate,
		endDate: $endDate,
		minDate: $minDate,
		isInvalidDate: function(date) {
			if(shift == 0) return (date.day() == 0 || date.day() == 6 || $.inArray(date.format('YYYY-MM-DD'), tgllibur) > -1);
		}
	}, function(start, end, label) {
		
		var start = moment(start),
			end = moment(end),
			countDateIn = end.diff(start, 'days')+1, //include weekends for shift
			countDateEx = dateDifference(start, end); //exclude weekends for non shift
			
			if(shift == 0){ //if non shift
				$('span.cutiHari').html(countDateEx);
				$('#qty').val(countDateEx);
			}else{ //shift
				$('span.cutiHari').html(countDateIn);
				$('#qty').val(countDateIn);
			}
	});
}

function dateDifference(start, end) {
	
	var excludeWeekends = moment().isoWeekdayCalc({  
		rangeStart: start,  
		rangeEnd: end,  
		weekdays: [1,2,3,4,5], //weekdays Mon to Fri
		exclusions: tgllibur  //public holidays
	});
	
	return excludeWeekends;
}

function disabledEnableCuti($val, check){
	
	if($val == 1 || $val == 2){
		$('input[name=shift]').iCheck('disable');
		$('input:radio[name="shift"]').filter('[value="0"]').iCheck('uncheck');
		$('input:radio[name="shift"]').filter('[value="1"]').iCheck('check');
		dateRefresh(1, null, $val);
	}else{
		$('input[name=shift]').iCheck('enable');
		$('input:radio[name="shift"]').filter('[value="1"]').iCheck('uncheck');
		$('input:radio[name="shift"]').filter('[value="0"]').iCheck('check');
		if(check !== 1) dateRefresh(0, null, $val);
	}
}