<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|oks
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group( ['middleware' => ['auth']], function() {
	
	//cuti
	Route::get('cutis/report', 'CutiController@reportCuti')->name('reportCuti');
	Route::get('cutis/updatecuti', 'CutiController@updateCuti');
	Route::get('cutis/{cuti}/view', ['as' => 'view/cuti', 'uses' => 'CutiController@view']);
	Route::post('cutis/status', ['as' => 'status/cuti', 'uses' => 'CutiController@status']);
	Route::get('cutis/{cuti}/letter', ['as' => 'letter/cuti', 'uses' => 'CutiController@letter']);
	
	//eprescribings
	Route::get('eprescribings/{id}/history/{no_reg?}', 'EprescribingController@history')->name('historyEprescribing');
	Route::get('eprescribings/{id}/order/{no_order?}', 'EprescribingController@create')->name('orderEprescribing');
	Route::post('eprescribings/{id}/order/{no_order?}', 'EprescribingController@store')->name('orderEprescribing');
	
	//HAIs
	Route::get('hais/report', 'HaisController@reportHais')->name('reportHais');
	
	//resource
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::resource('posts', 'PostController');
	Route::resource('comments', 'CommentController');
	Route::resource('cutis', 'CutiController');
	Route::resource('eprescribings', 'EprescribingController');
	Route::resource('hais', 'HaisController');
	//Route::resource('masters', 'MasterController');
	
	//add new
	Route::get('/', 'HomeController@index')->name('home');
});

//without Auth
Route::get('/home', 'HomeController@index')->name('home');
Route::get('cutis/{cuti}/approval', ['as' => 'approval/cuti', 'uses' => 'CutiController@approval']);

//Request is Ajax
Route::get('getBagian',['as'=>'getBagian','uses'=>'AjaxController@getBagian']);
Route::get('getSubagSeksi',['as'=>'getSubagSeksi','uses'=>'AjaxController@getSubagSeksi']);
Route::get('getInstalasi',['as'=>'getInstalasi','uses'=>'AjaxController@getInstalasi']);
Route::get('getBarangFarmasi',['as'=>'getBarangFarmasi','uses'=>'AjaxController@getBarangFarmasi']);
Route::get('getDataPasien',['as'=>'getDataPasien','uses'=>'AjaxController@getDataPasien']);
Route::get('getICD',['as'=>'getICD','uses'=>'AjaxController@getICD']);
Route::get('getDataHAIs',['as'=>'getDataHAIs','uses'=>'AjaxController@getDataHAIs']);

//DataTables
Route::post('getDataCuti',['as'=>'getDataCuti','uses'=>'DataTablesController@getDataCuti']);
Route::post('getReportHais',['as'=>'getReportHais','uses'=>'DataTablesController@getReportHais']);
Route::post('getDataUser',['as'=>'getDataUser','uses'=>'DataTablesController@getDataUser']);
