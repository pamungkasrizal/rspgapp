<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubagseksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subagseksis', function (Blueprint $table) {
            $table->integer('id_direktorat')->unsigned();
            $table->integer('id_bagian')->unsigned();
            $table->increments('id');
            $table->string('name', 191);
            $table->timestamps();

            $table->foreign('id_bagian')
                ->references('id')
                ->on('bagians')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subagseksis');
    }
}
