<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            //$table->increments('id');
            $table->string('id', 37)->primary();
			$table->string('name');
            $table->string('nip', 18)->unique();
            $table->string('password');
            $table->rememberToken();
			$table->integer('id_direktorat')->unsigned()->nullable();
            $table->integer('id_bagian')->unsigned()->nullable();
            $table->integer('id_subagseksi')->unsigned()->nullable();
            $table->integer('id_instalasi')->unsigned()->nullable();
            $table->integer('id_kastaff')->unsigned()->nullable();
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
