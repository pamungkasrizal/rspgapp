<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_params', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('xcond', 64);
            $table->integer('sq_id');
            $table->char('char_id', 16);
            $table->string('description', 1024);
            $table->string('additional', 256);
            $table->string('additional_2', 256);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_params');
    }
}
