			<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
				<div class="menu_section">
					<h3>General</h3>
					<ul class="nav side-menu">

						{{-- @if (Auth::user()->hasRole('User')) --}}
						@if (Auth::check())
							
							@if (Auth::user()->hasRole('Admin'))
							<li class="{{ Request::is('users*') || Request::is('roles*') ? 'active' : '' }}">
								<a><i class="fa fa-lock"></i> Admin App <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="{{ Request::is('users*') || Request::is('roles*') ? 'display:block' : '' }}">
									<li class="{{ Request::is('roles*') ? 'current-page' : '' }}">
										<a href="{{ Route('roles.index') }}">Roles</a>
									</li>
									<li class="{{ Request::is('users*') ? 'current-page' : '' }}">
										<a href="{{ Route('users.index') }}">Users</a>
									</li>
								</ul>
							</li>
							
							<li class="{{ Request::is('eprescribings*') ? 'active' : '' }}">
								<a href="{{ Route('eprescribings.index') }}">
									<i class="fa fa-user-md"></i> e-Prescribing
								</a>
							</li>
							
							<li class="{{ Request::is('hais*') ? 'active' : '' }}">
								<a><i class="fa fa-medkit"></i> Surveilans HAIs <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="{{ Request::is('hais*') ? 'display:block' : '' }}">
									<li class="{{ Request::is('hais/') ? 'current-page' : '' }}">
										<a href="{{ Route('hais.index') }}">Dashboard</a>
									</li>
									<li class="{{ Request::is('hais/create') ? 'current-page' : '' }}">
										<a href="{{ Route('hais.create') }}">Create Surveilans</a>
									</li>
									<li class="{{ Request::is('hais/') ? 'current-page' : '' }}">
										<a href="{{ Route('reportHais') }}">Report</a>
									</li>
								</ul>
							</li>
							@endif
							
							@can('view_cutis')
							<li class="{{ Request::is('cutis*') ? 'active' : '' }}">
								<a><i class="fa fa-cc"></i> Cuti App <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="{{ Request::is('cutis*') ? 'display:block' : '' }}">
									<li class="{{ Request::is('cutis/') ? 'current-page' : '' }}">
										<a href="{{ Route('cutis.index') }}">Cuti</a>
									</li>
									
									@if (Auth::user()->hasRole('Admin') || Auth::user()->id_bagian == NULL || Auth::user()->id == '3f920fe0-7674-42cb-bd4f-e937260f8797')
									<li class="{{ Request::is('cutis/') ? 'current-page' : '' }}">
										<a href="{{ route('reportCuti') }}">Report</a>
									</li>
									@endif
									
								</ul>
							</li>
							@endcan
							
						@endif
					</ul>
				</div>
				<div class="menu_section">
					<h3>Live On</h3>
					<ul class="nav side-menu">                  
						<li>
							<a href="javascript:void(0)">
								<i class="fa fa-laptop"></i> Landing Page 
								<span class="label label-success pull-right">Coming Soon</span>
							</a>
						</li>
					</ul>
				</div>
			</div>