<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
		@yield('title') | RSPGapp
	</title>

    <!-- Bootstrap -->
    <link href="{{ asset('assets/vendors-gentelella/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('assets/vendors-gentelella/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('assets/vendors-gentelella/iCheck/skins/flat/green.css') }}" rel="stylesheet">
	
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('assets/vendors-gentelella/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('assets/vendors-gentelella/build/css/custom.min.css') }}" rel="stylesheet">
	
	<!-- Datatables -->
    <link href="{{ asset('assets/vendors-gentelella/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors-gentelella/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors-gentelella/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors-gentelella/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors-gentelella/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
	
	<!-- Styles from content -->
	@yield('header_styles')
	
	<!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
		var $token_public = "{{ csrf_token() }}",
			$baseUrl = "{{ URL::to('/') }}";
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fa fa-stethoscope"></i> <span>RSPGapp</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="{{ asset('assets/images/doctor.png') }}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Selamat Datang,</span>
                <h2>{{ Auth::user()->name }}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
			@include('layouts._sidebar_menu')
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            @include('layouts._sidebar_footer')
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
			@include('layouts._top_navigation')
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
		
			<!-- top tiles -->
			<!-- /top tiles -->
			
			<!-- breadcrumb -->
			@if (Request::is('/') == '' && Request::is('home') == '')
				<div class="page-title">
					<div class="title_left">
						<h3>
							@yield('breadcrumb')
							<small>@yield('breadcrumbSmall')</small>
						</h3>
					</div>

					<div class="title_right">
						<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right">
							<a href="{{ url()->previous() }}" style="float: inherit;">
								<i class="fa fa-hand-o-left" style="font-size: 2em;"></i> Back
							</a>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			@endif
			<!-- /breadcrumb -->
			
			<!-- inti page content -->
			<div id="flash-msg">
                @include('flash::message')
            </div>
			
            @yield('content')
			<!-- /inti page content -->
		  
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Powered by Bootstrap Gentelella - IT RSPG <i class="fa fa-copyright"></i> Nov 2017
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('assets/vendors-gentelella/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('assets/vendors-gentelella/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('assets/vendors-gentelella/fastclick/lib/fastclick.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('assets/vendors-gentelella/iCheck/icheck.min.js') }}"></script>
    <!-- Skycons -->
    <script src="{{ asset('assets/vendors-gentelella/skycons/skycons.js') }}"></script>
    <!-- DateJS -->
    <script src="{{ asset('assets/vendors-gentelella/DateJS/build/date.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('assets/vendors-gentelella/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/moment/min/moment-weekday-calc.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ asset('assets/vendors-gentelella/build/js/custom.min.js') }}"></script>
	
	<!-- Datatables -->
    <script src="{{ asset('assets/vendors-gentelella/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/typeahead/bootstrap-typeahead.min.js') }}"></script>
	<script src="{{ asset('assets/js/AutoNumeric.js') }}"></script>
	<script src="{{ asset('assets/js/reCopy.js') }}"></script>
	<!-- Scripts from content -->
	@yield('footer_scripts')
	<script src="{{ asset('assets/vendors-gentelella/build/js/rspgapp.js') }}"></script>
  </body>
</html>