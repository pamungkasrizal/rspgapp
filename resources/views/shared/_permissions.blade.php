					<section id="web-application">
						<h2 class="page-header">{{ $title or 'Override Permissions' }}</h2>
						<div class="row fontawesome-icon-list">					
							<div class="row fontawesome-icon-list">
								@foreach($permissions as $perm)
									<?php
										$per_found = null;

										if( isset($role) ) {
											$per_found = $role->hasPermissionTo($perm->name);
										}

										if( isset($user)) {
											$per_found = $user->hasDirectPermission($perm->name);
										}
									?>

									<div class="fa-hover col-md-3 col-sm-4 col-xs-12">
										<div class="checkbox">
											<label class="{{ str_contains($perm->name, 'delete') ? 'text-danger' : '' }}">
												{!! Form::checkbox("permissions[]", $perm->name, $per_found, isset($options) ? $options : ['class' => 'flat']) !!} {{ $perm->name }}
											</label>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</section>