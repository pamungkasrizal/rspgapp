@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Master</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<!-- CODE HERE -->
					<!-- {!! Form::open(['route' => ['masters.store'], 'class' => 'form-horizontal form-label-left' ]) !!} CREATE -->
					{!! Form::model($master, ['method' => 'PUT', 'route' => ['masters.update',  $master->id ], 'class' => 'form-horizontal form-label-left' ]) !!}
						<div class="form-group @if ($errors->has('name')) has-error @endif">
							{!! Html::decode(Form::label('name','Name <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
							<div class="col-md-6 col-sm-6 col-xs-12">
								{!! Form::text('name', null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
							</div>
							@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('description')) has-error @endif">
							{!! Html::decode(Form::label('description','Description <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
							<div class="col-md-6 col-sm-6 col-xs-12">
								{!! Form::textarea('description', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Keterangan Cuti', 'rows' => '4']) !!}
							</div>
							@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
						</div>
						<!-- Submit Form Button -->
						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-9 text-right">
								<a class="btn btn-danger" href="{{ route('cutis.index') }}">Cancel</a>
								{!! Form::submit('Create', ['class' => 'btn btn-success']) !!}
							</div>
						</div>
					{!! Form::close() !!}
					
				</div>
			</div>
		</div>
	</div>
@endsection