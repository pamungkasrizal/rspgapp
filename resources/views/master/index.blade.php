@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Master</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<table>
						<tr>
							<td>Name</td>
							<td>Description</td>
						</tr>
						@if(!empty($data))
							@foreach($data as $val)
							<tr>
								<td>{{ $val->name }}</td>
								<td>{{ $val->description }}</td>
							</tr>
							@endforeach
						@endif
					</table>
					
				</div>
			</div>
		</div>
	</div>
@endsection