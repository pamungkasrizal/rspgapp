@extends('layouts.app')

@section('title', 'Cuti Dashboard')
@section('breadcrumb', 'Dashboard Cuti')
@section('breadcrumbSmall', '')

@section('content')
	
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Sisa Cuti Tahun {{ date('Y') }} : {{ Auth::user()->json_user['total'] }}</h2>
					<ul class="nav navbar-right panel_toolbox">
						@can('add_cutis')
						<li>
							<a href="{{ route('cutis.create') }}" style="color: #1ABB9C;font-weight: bold;">
								<i class="fa fa-plus-square"></i> ajukan cuti
							</a>
						</li>
						<li><a style="cursor: default;">|</a></li>
						@endcan
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<!-- CODE HERE -->
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Syarat-syarat Mengajukan Cuti Tahunan <small></small></h2>
								<ul class="nav navbar-right panel_toolbox">
									<li><a style="cursor: default; left:33px;">|</a></li>
									<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<div class="dashboard-widget-content">
									<ul class="list-unstyled timeline widget">
										<li>
											<div class="block">
												<div class="block_content">
													<h2 class="title" style="font-size: 15px;">
														<a>Pegawai Negeri Sipil yang telah bekerja sekurang-kurangnya 1 (satu) tahun secara terus menerus.</a>
													</h2>
													<!--<p class="excerpt">small text</p>-->
												</div>
											</div>
										</li>
										<li>
											<div class="block">
												<div class="block_content">
													<h2 class="title" style="font-size: 15px;">
														<a>Lamanya cuti tahunan adalah 12 (dua belas) hari kerja</a>
													</h2>
													<!--<p class="excerpt">small text</p>-->
												</div>
											</div>
										</li>
										<li>
											<div class="block">
												<div class="block_content">
													<h2 class="title" style="font-size: 15px;">
														<a>Cuti tahunan tidak dapat dipecah-pecah hingga jangka waktu yang kurang dari 3 (tiga) hari kerja</a>
													</h2>
													<!--<p class="excerpt">*minimal 3 hari kerja</p>-->
												</div>
											</div>
										</li>
										<li>
											<div class="block">
												<div class="block_content">
													<h2 class="title" style="font-size: 15px;">
														<a>Untuk mendapatkan cuti tahunan Pegawai Negeri Sipil bersangkutan mengajukan permintaan secara tertulis kepada pejabat yang berwenang memberikan cuti</a>
													</h2>
													<!--<p class="excerpt">*minimal 3 hari kerja</p>-->
												</div>
											</div>
										</li>
										<li>
											<div class="block">
												<div class="block_content">
													<h2 class="title" style="font-size: 15px;">
														<a>Cuti tahunan diberikan secara tertulis oleh pejabat yang berwenang memberikan cuti</a>
													</h2>
													<!--<p class="excerpt">*minimal 3 hari kerja</p>-->
												</div>
											</div>
										</li>
										<li>
											<div class="block">
												<div class="block_content">
													<h2 class="title" style="font-size: 15px;">
														<a>Cuti tahunan yang akan di jalankan ditempat yang sulit perhubungannya, maka jangka waktu cuti tahunan tersebut dapat ditambah untuk paling lama 14 (empat belas) hari</a>
													</h2>
													<!--<p class="excerpt">*minimal 3 hari kerja</p>-->
												</div>
											</div>
										</li>
										<li>
											<div class="block">
												<div class="block_content">
													<h2 class="title" style="font-size: 15px;">
														<a>Cuti tahunan yang tidak diambil dalam tahun yang bersangkutan dapat diambil dalam tahun berikutnya untuk palinh lama 18 (delapan belas) hari kerja termasuk cuti tahunan dalam tahun yang sedang berjalan.</a>
													</h2>
													<!--<p class="excerpt">*minimal 3 hari kerja</p>-->
												</div>
											</div>
										</li>
										<li>
											<div class="block">
												<div class="block_content">
													<h2 class="title" style="font-size: 15px;">
														<a>Cuti tahunan yang tidak diambil lebih dari 2 (dua) tahun berturut-turut, dapat diambil dalam tahun berikutnya untuk paling lama 24 (dua puluh empat) hari kerja termasuk cuti tahunan dalam tahun yang sedang berjalan</a>
													</h2>
													<!--<p class="excerpt">*minimal 3 hari kerja</p>-->
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
						
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>History Cuti <small></small></h2>
								<ul class="nav navbar-right panel_toolbox">
									<li><a style="cursor: default; left:33px;">|</a></li>
									<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Tanggal</th>
											<th>Pelaksana PLT</th>
											<th>Atasan Langsung</th>
											<th>Direksi</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										@forelse($history as $valHis)
												@php 
													$iread = $valHis->implementer['read'];
													$iappr = $valHis->implementer['approve'];
													$hread = $valHis->head['read'];
													$happr = $valHis->head['approve'];
													$dread = $valHis->json_data['read'];
													$dappr = $valHis->json_data['approve'];
												@endphp
											<tr>
												<td>
													<a href="{{ route('cutis.edit', $valHis->id) }}" style="color:blue;" title="Edit">
														{{ date('d M Y', strtotime($valHis->start_date)) }} - 
														{{ date('d M Y', strtotime($valHis->end_date)) }}
													</a>
												</td>
												<td>
													<button type="button" class="btn {{ ($iread == true) ? 'btn-success' : 'btn-grey' }} btn-xs">
														{{ ($iread == true) ? 'read' : 'unread' }}
													</button>
													<button type="button" class="btn {{ ($iappr == true ? 'btn-success' : ($iappr == false && !empty($valHis->implementer['note']) ? 'btn-danger' : 'btn-grey')) }} btn-xs">
														{{ ($iappr == true ? 'approved' : ($iappr == false && !empty($valHis->implementer['note']) ? 'reject' : 'wait')) }}
													</button>
												</td>
												<td>
													<button type="button" class="btn {{ ($hread == true) ? 'btn-success' : 'btn-grey' }} btn-xs">
														{{ ($hread == true) ? 'read' : 'unread' }}
													</button>
													<button type="button" class="btn {{ ($happr == true ? 'btn-success' : ($happr == false && !empty($valHis->head['note']) ? 'btn-danger' : 'btn-grey')) }} btn-xs">
														{{ ($happr == true ? 'approved' : ($happr == false && !empty($valHis->head['note']) ? 'reject' : 'wait')) }}
													</button>
												</td>
												<td>
													<button type="button" class="btn {{ ($dread == true) ? 'btn-success' : 'btn-grey' }} btn-xs">
														{{ ($dread == true) ? 'read' : 'unread' }}
													</button>
													<button type="button" class="btn {{ ($dappr == true ? 'btn-success' : ($dappr == false && !empty($valHis->json_data['note']) ? 'btn-danger' : 'btn-grey')) }} btn-xs">
														{{ ($dappr == true ? 'approved' : ($dappr == false && !empty($valHis->json_data['note']) ? 'reject' : 'wait')) }}
													</button>
												</td>
												<td>
													@if($dappr == true) 
														<a href="{{ route('letter/cuti', $valHis->id) }}" target="_blank" style="color: #1ABB9C;font-weight: bold;" title="Surat Izin Cuti">
															<i class="fa fa-envelope" style="font-size: large;"></i> SIC
														</a>
													@endif
												</td>
											</tr>
										@empty
											<tr>
												<td colspan="3" class="text-center">Data is empty !!</td>
											</tr>
										@endforelse
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
					@if(count($implementer) > 0)
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Pelaksana PLT <small></small></h2>
								<ul class="nav navbar-right panel_toolbox">
									<li><a style="cursor: default; left:33px;">|</a></li>
									<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Nama</th>
											<th>Tanggal</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										@forelse($implementer as $valImp)
											<tr>
												<th>{{ $valImp->user->name }}</th>
												<td>
													{{ date('d M Y', strtotime($valImp->start_date)) }} - 
													{{ date('d M Y', strtotime($valImp->end_date)) }}
												</td>
												<td class="text-center">
													<a href="{{ route('view/cuti', $valImp->id) }}">
														<button type="button" class="btn btn-info btn-xs">view</button>
													</a>
												</td>
											</tr>
										@empty
											<tr>
												<td colspan="3" class="text-center">Data is empty !!</td>
											</tr>
										@endforelse
									</tbody>
								</table>
							</div>
						</div>
					</div>
					@endif
					
					@if(Auth::user()->id_kastaff == 1 && count($head) > 0)
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Persetujuan Atasan Langsung <small></small></h2>
								<ul class="nav navbar-right panel_toolbox">
									<li><a style="cursor: default; left:33px;">|</a></li>
									<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Nama</th>
											<th>Tanggal</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										@forelse($head as $valHead)
											<tr>
												<th>{{ $valHead->user->name }}</th>
												<td>
													{{ date('d M Y', strtotime($valHead->start_date)) }} - 
													{{ date('d M Y', strtotime($valHead->end_date)) }}
												</td>
												<td class="text-center">
													<a href="{{ route('view/cuti', $valHead->id) }}">
														<button type="button" class="btn btn-info btn-xs">view</button>
													</a>
												</td>
											</tr>
										@empty
											<tr>
												<td colspan="3" class="text-center">Data is empty !!</td>
											</tr>
										@endforelse
									</tbody>
								</table>
							</div>
						</div>
					</div>
					@endif
					
					@if((Auth::user()->hasRole('Admin') || $direksi->status == true) && count($kepeg) > 0)
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Persetujuan Direksi <small></small></h2>
								<ul class="nav navbar-right panel_toolbox">
									<li><a style="cursor: default; left:33px;">|</a></li>
									<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Nama</th>
											<th>Tanggal</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										@forelse($kepeg as $valKepeg)
											<tr>
												<th>{{ $valKepeg->user->name }}</th>
												<td>
													{{ date('d M Y', strtotime($valKepeg->start_date)) }} - 
													{{ date('d M Y', strtotime($valKepeg->end_date)) }}
												</td>
												<td class="text-center">
													<a href="{{ route('view/cuti', $valKepeg->id) }}">
														<button type="button" class="btn btn-info btn-xs">view</button>
													</a>
												</td>
											</tr>
										@empty
											<tr>
												<td colspan="3" class="text-center">Data is empty !!</td>
											</tr>
										@endforelse
									</tbody>
								</table>
							</div>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection