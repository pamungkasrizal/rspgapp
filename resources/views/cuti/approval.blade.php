<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Approval Cuti | RSPGapp</title>
	<link rel="shortcut icon" href="">
	<!-- Bootstrap-->
	<link href="{{ asset('assets/vendors-gentelella/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="{{ asset('assets/vendors-gentelella/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
	<!-- Animate.css -->
	<link href="{{ asset('assets/vendors-gentelella/animate.css/animate.min.css') }}" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="{{ asset('assets/vendors-gentelella/build/css/custom.min.css') }}" rel="stylesheet">
</head>

<body class="login">
	<div>
		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form>
						<h1>Approval Cuti</h1>
						<div class="profile clearfix">
							<div class="row top_tiles">
							<div style="text-align: left;" class="animated flipInY">
								<div class="tile-stats">
									<div class="icon"><i style="color: rgb(26, 187, 156);" class="fa fa-check-square-o"></i></div>
										<div class="count">Approved</div>
										<h3>{{ date('d/m/Y', strtotime($data->start_date)).' - '.date('d/m/Y', strtotime($data->end_date)).' ('.$data->qty.' days)' }}</h3>
										<p>NIP/NPP : {{ $data->user->nip }}</p>
										<p>{{ $data->user->name }}</p>
										<p style="color: rgb(26, 187, 156);">{{ $data->shift == 1 ? 'Shift' : 'Non Shift' }}</p>
									</div>
								</div>
							</div>
						</div>

						<div class="clearfix"></div>

						<div class="separator">
							<div>
								<h1><i class="fa fa-stethoscope"></i> RSPGapp</h1>
								<p>©2017 November, All Rights Reserved. </br>RSPGapp is a Bootstrap 3 template. Privacy and Terms</p>
							</div>
						</div>
					</form>
				</section>
			</div>
			
		</div>
	</div>
	
	<!-- iCheck -->
    <script src="{{ asset('assets/vendors-gentelella/jquery/dist/jquery.min.js') }}"></script>
</body>
</html>