@extends('layouts.app')

@section('title', 'Cuti')
@section('breadcrumb', 'Cuti')
@section('breadcrumbSmall', '')

@section('content')
			<div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <small> </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
						<li><a style="cursor: default; left:33px;">|</a></li>
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
							<div class="col-sm-1" style="padding-left:0;">
								<img src="{{ asset('assets/images/Logo-Kemenkes-2017-1.png') }}" style="width: 80px;height: 80px;">
							</div>
							<div class="col-sm-10 text-center" style="border-bottom: 2px solid #ddd;">
								<p style="font-size:18px;margin-bottom: 0px;letter-spacing: 6px;">LAMPIRAN II</p>
								<p style="font-size:18px;margin-bottom: 0px;letter-spacing: 6px;">SURAT EDARAN KEPALA</p>
								<p style="font-size:18px;letter-spacing: 6px;">BADAN ADMINISTRASI KEPEGAWAIAN NEGARA</p>
							</div>
							<div class="col-sm-1" style="padding-right:0;">
								<img src="{{ asset('assets/images/logo-rspg.png') }}" style="width: 100px;height: 65px;margin-top: 10px;" class="pull-right">
							</div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info" style="padding-top:15px;">
                        <div class="col-sm-4 invoice-col"></div>
                        <div class="col-sm-4 invoice-col"></div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Cisarua, {{ date('d F Y', strtotime($datacuti->created_at)) }}
                          <address>
							  Yang Terhormat,
							  <br>Direktur Utama
							  <br>RSP Dr. M. Goenawan Partowidigdo
							  <br>Melalui
							  <br>Ka.Sub.Bag TU dan Kepegawaian
							  <br>Di -
							  <br>Tempat
						  </address>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table">
							<p>Yang bertanda tangan dibawah ini :</p>
							<table class="countries_list">
								<tbody>
									<tr>
										<td width="25%">Nama</td>
										<td width="2%">:</td>
										<td>{{ $datacuti->user->name }}</td>
									</tr>
									<tr>
										<td>NIP/NPP</td>
										<td>:</td>
										<td>{{ $datacuti->user->nip }}</td>
									</tr>
									<tr>
										<td>Pangkat Gol. Ruang</td>
										<td>:</td>
										<td>{{ $datacuti->user->json_data['pangkat'] }} - {{ $datacuti->user->json_data['golongan'] }}</td>
									</tr>
									<tr>
										<td>Jabatan</td>
										<td>:</td>
										<td>{{ $datacuti->user->json_data['jabatan'] }}</td>
									</tr>
									<tr>
										<td>Satuan Organisasi</td>
										<td>:</td>
										<td>RSP Dr. M. Goenawan Partowidigdo Cisarua Bogor</td>
									</tr>
									<tr>
										<td><b>Jenis Cuti</b></td>
										<td><b>:</b></td>
										<td><b>{{ $cutiType->description }}</b></td>
									</tr>
									<tr>
										<td><b>Keterangan Cuti</b></td>
										<td><b>:</b></td>
										<td><b>{{ $datacuti->reason }}</b></td>
									</tr>
									<tr>
										<td><b>Permohonan cuti untuk tahun</b></td>
										<td><b>:</b></td>
										<td><b>{{ date('Y', strtotime($datacuti->start_date)) }}</b></td>
									</tr>
									<tr>
										<td><b>Selama</b></td>
										<td><b>:</b></td>
										<td><b>{{ $datacuti->qty }} hari kerja ({{ ($datacuti->shift == 0) ? 'Non Shift' : 'Shift' }})</b></td>
									</tr>
									<tr>
										<td><b>Tanggal</b></td>
										<td><b>:</b></td>
										<td><b>{{ date('d F Y', strtotime($datacuti->start_date)) }} s/d {{ date('d F Y', strtotime($datacuti->end_date)) }}</b></td>
									</tr>
								</tbody>
							</table>
							<div style="padding-top:20px;">
								<p>
									Demikian permintaan ini saya buat, untuk dapat di pertimbangkan sebagaimana mestinya.
								</p>
							</div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
					  
					  <div class="row invoice-info" style="padding-top:15px;">
                        <div class="col-sm-3 invoice-col">
                          <b>Hormat Saya,</b>
                          <address>
							  <br>
							  <br>
							  <br>
							  <br><b style="border-bottom: 1px solid #ddd;">{{ $datacuti->user->name }}</b>
							  <br><b>NIP/NPP : {{ $datacuti->user->nip }}</b>
						  </address>
                        </div>
						<div class="col-sm-3 invoice-col">
                          <b>Pelaksana PLT,</b>
							<address>
							  @if(Auth::user()->id == $datacuti->implementer['id_user'] && $datacuti->implementer['note'] == '')
								{!! Form::open(['route' => ['status/cuti']]) !!}
								  <div class="form-group @if ($errors->has('note')) has-error @endif">
								  <textarea class="form-control" style="width:80%; margin-bottom:10px;" name="note">-</textarea>
								  <input type="hidden" name="field" value="implementer">
								  <input type="hidden" name="id_cuti" value="{{ $datacuti->id }}">
								  @if ($errors->has('note')) <p class="help-block">{{ $errors->first('note') }}</p> @endif
								  <button type="submit" name="rea" value="0" class="btn btn-danger btn-sm">Reject</button>
								  <button type="submit" name="rea" value="1" class="btn btn-success btn-sm">Approve</button>
								  </div>
								  {!! Form::close() !!}
							  @else 
							  <div style="height: 57px; padding-top: 10px;">
								<span style="color:{{ ($datacuti->implementer['approve'] == true) ? '#26B99A' : '#d9534f' }}">{{ $datacuti->implementer['note'] }}</span>
							  </div>
							  @endif
							  <br><b style="border-bottom: 1px solid #ddd;">{{ $imp->name }}</b>
							  <br><b>NIP/NPP : {{ $imp->nip }}</b>
							</address>
                        </div>
						<div class="col-sm-3 invoice-col">
                          <b>Atasan Langsung,</b>
							<address>
							  @if(Auth::user()->id == $datacuti->head['id_user'] && $datacuti->head['note'] == '')
								{!! Form::open(['route' => ['status/cuti']]) !!}
								  <div class="form-group @if ($errors->has('note')) has-error @endif">
								  <textarea class="form-control" style="width:80%; margin-bottom:10px;" name="note">-</textarea>
								  <input type="hidden" name="field" value="head">
								  <input type="hidden" name="id_cuti" value="{{ $datacuti->id }}">
								  @if ($errors->has('note')) <p class="help-block">{{ $errors->first('note') }}</p> @endif
								  <button type="submit" name="rea" value="0" class="btn btn-danger btn-sm">Reject</button>
								  <button type="submit" name="rea" value="1" class="btn btn-success btn-sm">Approve</button>
								  </div>
								  {!! Form::close() !!}
							  @else 
							  <div style="height: 57px; padding-top: 10px;">
								<span style="color:{{ ($datacuti->head['approve'] == true) ? '#26B99A' : '#d9534f' }}">{{ $datacuti->head['note'] }}</span>
							  </div>
							  @endif
							  <br><b style="border-bottom: 1px solid #ddd;">{{ $hea->name }}</b>
							  <br><b>NIP/NPP : {{ $hea->nip }}</b>
							</address>
                        </div>
						<div class="col-sm-3 invoice-col">
                          <b>Direksi,</b>
							<address>
							  @if((Auth::user()->hasRole('Admin') || $dir->id == Auth::user()->id) && $datacuti->json_data['approve'] == false && $datacuti->head['approve'] == true)
								{!! Form::open(['route' => ['status/cuti']]) !!}
								  <div class="form-group @if ($errors->has('note')) has-error @endif">
								  <textarea class="form-control" style="width:80%; margin-bottom:10px;" name="note">-</textarea>
								  <input type="hidden" name="field" value="json_data">
								  <input type="hidden" name="id_cuti" value="{{ $datacuti->id }}">
								  @if ($errors->has('note')) <p class="help-block">{{ $errors->first('note') }}</p> @endif
								  <button type="submit" name="rea" value="0" class="btn btn-danger btn-sm">Reject</button>
								  <button type="submit" name="rea" value="1" class="btn btn-success btn-sm">Approve</button>
								  </div>
								{!! Form::close() !!}
							  @else 
							  <div style="height: 57px; padding-top: 10px;">
								<span style="color:{{ ($datacuti->json_data['approve'] == true) ? '#26B99A' : '#d9534f' }}">
									{{ $datacuti->json_data['note'] }}
								</span>
							  </div>
							  @endif
							  <br><b style="border-bottom: 1px solid #ddd;">{{ $dir->name }}</b>
							  <br><b>NIP/NPP : {{ $dir->nip }}</b>
							</address>
                        </div>
                        <!-- /.col -->
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
@endsection