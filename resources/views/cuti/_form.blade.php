<!-- NIP/NPP Form Input -->
<div class="form-group @if ($errors->has('nip')) has-error @endif">
	{!! Html::decode(Form::label('nip','NIP/NPP <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
	<div class="col-md-6 col-sm-6 col-xs-12">
		{!! Form::text('nip', Auth::user()->nip, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'NIP/NPP', 'readonly' => 'true']) !!}
		{!! Form::hidden('id_user', Auth::user()->id) !!}
	</div>
    @if ($errors->has('nip')) <p class="help-block">{{ $errors->first('nip') }}</p> @endif
</div>

<!-- name Form Input -->
<div class="form-group @if ($errors->has('name')) has-error @endif">
    {!! Html::decode(Form::label('name','Nama <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
	<div class="col-md-6 col-sm-6 col-xs-12">
		{!! Form::text('name', Auth::user()->name, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Nama', 'readonly' => 'true']) !!}
	</div>
    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
</div>

<!-- type Form Input -->
<div class="form-group @if ($errors->has('type')) has-error @endif">
    {!! Html::decode(Form::label('type','Jenis Cuti <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
	<div class="col-md-6 col-sm-6 col-xs-12">
		{!! Form::select('type', $cutiType, null, ['class' => 'select2_group form-control', 'placeholder' => '-- Pilih Jenis Cuti --']) !!}
	</div>
    @if ($errors->has('type')) <p class="help-block">{{ $errors->first('type') }}</p> @endif
</div>

<!-- shift Form Input -->
<div class="form-group @if ($errors->has('shift')) has-error @endif">
    {!! Html::decode(Form::label('shift','Jenis Karyawan <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="radio">
			<label>
				{!! Form::radio('shift', '0', true, ['class' => 'flat']) !!} Non Shift
			</label>
			<label>
				{!! Form::radio('shift', '1', false, ['class' => 'flat']) !!} Pola Shift
			</label>
		</div>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-12">
		<span class="form-control-feedback right" aria-hidden="true" style="width: 67%;">Sisa Cuti : {{ $totalCuti }} Hari</span>
	</div>
</div>

<!-- date Form Input -->
<div class="form-group @if ($errors->has('qty')) has-error @endif">
	{!! Html::decode(Form::label('date','Tanggal Cuti <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="input-prepend input-group">
			<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
			{!! Form::text('date', null, ['class' => 'form-control', 'style' => 'width: 200px;', 'id' => 'reservation']) !!}
			<span class="form-control-feedback right cutiHari" aria-hidden="true" style="right: -5px">0</span>
			{!! Form::hidden('qty', $qty_var, ['id' => 'qty']) !!}
		</div>
	</div>
	@if ($errors->has('qty')) <p class="help-block">{{ $errors->first('qty') }}</p> @endif
</div>

<!-- reason Form Input -->
<div class="form-group @if ($errors->has('reason')) has-error @endif">
    {!! Html::decode(Form::label('reason','Keterangan Cuti <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
	<div class="col-md-6 col-sm-6 col-xs-12">
		{!! Form::textarea('reason', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Keterangan Cuti', 'rows' => '4']) !!}
	</div>
    @if ($errors->has('reason')) <p class="help-block">{{ $errors->first('reason') }}</p> @endif
</div>

<!-- implementer Form Input -->
<div class="form-group @if ($errors->has('implementer')) has-error @endif">
    {!! Html::decode(Form::label('implementer','Pelaksana Tugas <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
	<div class="col-md-6 col-sm-6 col-xs-12">
		{!! Form::select('implementer', $implementer, null, ['class' => 'select2_group form-control', 'placeholder' => '-- Pilih Pelaksana Tugas --']) !!}
	</div>
    @if ($errors->has('implementer')) <p class="help-block">{{ $errors->first('implementer') }}</p> @endif
</div>

<!-- head Form Input -->
<div class="form-group @if ($errors->has('head')) has-error @endif">
    {!! Html::decode(Form::label('head','Atasan Langsung <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
	<div class="col-md-6 col-sm-6 col-xs-12">
		{!! Form::select('head', $head, null, ['class' => 'select2_group form-control', 'placeholder' => '-- Pilih Atasan Langsung --']) !!}
	</div>
    @if ($errors->has('head')) <p class="help-block">{{ $errors->first('head') }}</p> @endif
</div>