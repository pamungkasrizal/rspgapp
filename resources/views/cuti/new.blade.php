@extends('layouts.app')

@section('title', 'Ajukan Cuti')
@section('breadcrumb', '')
@section('breadcrumbSmall', '')

@section('content')
	
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Ajuan Cuti</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<!-- CODE HERE -->
					{!! Form::open(['route' => ['cutis.store'], 'class' => 'form-horizontal form-label-left' ]) !!}
						@include('cuti._form', ['qty_var' => 1])
						<!-- Submit Form Button -->
						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-9 text-right">
								<a class="btn btn-danger" href="{{ route('cutis.index') }}">Cancel</a>
								{!! Form::submit('Create', ['class' => 'btn btn-success']) !!}
							</div>
						</div>
					{!! Form::close() !!}
					
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer_scripts')
	<script>
			var tgllibur = <?php print_r($tglmerah); ?>;
	</script>
@endsection