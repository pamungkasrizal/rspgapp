@extends('layouts.app')

@section('title', 'Cuti Report')
@section('breadcrumb', 'Report Cuti')
@section('breadcrumbSmall', '')

@section('content')
	
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<ul class="nav navbar-right panel_toolbox">
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<!-- CODE HERE -->						
					<table class="table table-striped table-bordered table-hover" id="dTReportCuti">
						<thead>
							<tr>
								<th>Nama</th>
								<th>NIP / NPP</th>
								<th>Jabatan</th>
								<th>Tanggal Cuti</th>
								<th>Qty</th>
								<th>Type</th>
								<th>Pola Shift</th>
								<th>Direksi</th>
							</tr>
						</thead>
					</table>
					
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer_scripts')
<script>
	$(document).ready(function () {
		var $type = ['Cuti Bersalin','Cuti Besar','Cuti Karena Alasan Penting','Cuti Sakit/Penting','Cuti Tahunan'],
			$shift = ['Non Shift', 'Shift'];
			
        $('#dTReportCuti').DataTable({
            "processing": true,
            "serverSide": true,
			"pageLength": 10,
            "ajax":{
                     "url": "{{ route('getDataCuti') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "user.name" },
                { "data": "user.nip" },
                { "data": "user.json_data.jabatan" },
				{ "data": "tgl_cuti",
					"render":function(data, type, full, meta){
					   return moment(full.start_date).format('DD/MM/YYYY') +' s/d '+ moment(full.end_date).format('DD/MM/YYYY');
					}
				},
                { "data": "qty",
					"render":function(data, type, full, meta){
					   return full.qty+' hari';
					}
				},
                { "data": "type", 
					"render":function(data, type, full, meta){
					   return $type[full.type - 1];
					}
				},
                { "data": "shift",
					"render":function(data, type, full, meta){
					   return $shift[full.shift];
					}
				},
                { "data": "direksi",
					"render":function(data, type, full, meta){
					   $html = '<a href="'+full.id+'/view"><button type="button" class="btn btn-info btn-xs">view</button></a>';
					   $html += '<button type="button" class="btn '+(full.json_data.read == true  ? 'btn-success' : 'btn-grey')+' btn-xs">'+(full.json_data.read == true  ? 'read' : 'unread')+'</button>';
					   $html += '<button type="button" class="btn '+(full.json_data.approve == true ? 'btn-success' : (full.json_data.approve && full.json_data.note !== '' ? 'btn-danger' : 'btn-grey'))+' btn-xs">'+(full.json_data.approve == true && full.json_data.note !== ''  ? 'approved' : (full.json_data.approve && full.json_data.note !== '' ? 'reject' : 'wait'))+'</button>';
					   
					   if(full.json_data.approve == true){
						   $html += '<a href="'+full.id+'/letter" target="_blank" style="color: #1ABB9C;font-weight: bold;" title="Surat Izin Cuti"><i class="fa fa-envelope" style="font-size: large;"></i> SIC</a>';
					   }
					   
					   return $html;
					}
				} 
            ]
        });
    });
</script>
@endsection