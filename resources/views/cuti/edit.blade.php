@extends('layouts.app')

@section('title', 'Edit Cuti ')
@section('breadcrumb', 'Edit')
@section('breadcrumbSmall', 'Cuti')

@section('content')
	
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Cuti</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<!-- CODE HERE -->
					{!! Form::model($cuti, ['method' => 'PUT', 'route' => ['cutis.update',  $cuti->id ], 'class' => 'form-horizontal form-label-left' ]) !!}
						@include('cuti._form', ['qty_var' => null])
						<!-- Submit Form Button -->
						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-9 text-right">
								<a class="btn btn-danger" href="{{ route('cutis.index') }}">Cancel</a>
								{!! Form::submit('Save Changes', ['class' => 'btn btn-success']) !!}
							</div>
						</div>
					{!! Form::close() !!}
					
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer_scripts')
	<script>
			var tgllibur = <?php print_r($tglmerah); ?>;
	</script>
@endsection