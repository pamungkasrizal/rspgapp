@extends('layouts.app')

@section('title', 'Roles & Permissions')
@section('breadcrumb', 'Roles')
@section('breadcrumbSmall', 'Permissions')

@section('content')

    <!-- Modal -->
    <div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="roleModalLabel">
        <div class="modal-dialog" role="document">
            {!! Form::open(['method' => 'post']) !!}

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="roleModalLabel">Role</h4>
                </div>
                <div class="modal-body">
                    <!-- name Form Input -->
                    <div class="form-group @if ($errors->has('name')) has-error @endif">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Role Name', 'required']) !!}
                        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <!-- Submit Form Button -->
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
	
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>List Data</h2>
					<ul class="nav navbar-right panel_toolbox">
						@can('add_roles')
						<li>
							<a href="#" class="" data-toggle="modal" data-target="#roleModal" style="color: #1ABB9C;font-weight: bold;">
								<i class="fa fa-plus-square"></i> add new
							</a>
						</li>
						<li><a style="cursor: default;">|</a></li>
						@endcan
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					@forelse ($roles as $role)
						{!! Form::model($role, ['method' => 'PUT', 'route' => ['roles.update',  $role->id ], 'class' => 'm-b']) !!}

						@if($role->name === 'Admin')
							@include('shared._permissions', [
										  'title' => $role->name .' Permissions',
										  'options' => ['disabled', 'class' => 'flat'] ])
						@else
							@include('shared._permissions', [
										  'title' => $role->name .' Permissions',
										  'model' => $role ])
						  
							@can('edit_roles')
								<div class="ln_solid"></div>
								<div class="form-group">
									<div class="col-md-12 text-right">
										{!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
									</div>
								</div>
							@endcan
						@endif

						{!! Form::close() !!}

					@empty
						<p>No Roles defined, please run <code>php artisan db:seed</code> to seed some dummy data.</p>
					@endforelse
				</div>
			</div>
		</div>
	</div>
@endsection