<!-- Name Form Input -->
<div class="form-group @if ($errors->has('name')) has-error @endif">
	{!! Html::decode(Form::label('name','Name <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
	<div class="col-md-6 col-sm-6 col-xs-12">
		{!! Form::text('name', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Name']) !!}
	</div>
    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
</div>

<!-- email Form Input -->
<div class="form-group @if ($errors->has('email')) has-error @endif">
    {!! Html::decode(Form::label('email','Email <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
	<div class="col-md-6 col-sm-6 col-xs-12">
		{!! Form::text('email', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Email']) !!}
	</div>
    @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
</div>

<!-- password Form Input -->
<div class="form-group @if ($errors->has('password')) has-error @endif">
    {!! Html::decode(Form::label('password','Password <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
	<div class="col-md-6 col-sm-6 col-xs-12">
		{!! Form::text('password', '', ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Password']) !!}
	</div>
    @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
</div>

<!-- Roles Form Input -->
<div class="form-group @if ($errors->has('roles')) has-error @endif">
    {!! Html::decode(Form::label('roles[]', 'Roles <span class="required">*</span>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
	<div class="col-md-6 col-sm-6 col-xs-12">
		{!! Form::select('roles[]', $roles, isset($user) ? $user->roles->pluck('id')->toArray() : null,  ['class' => 'select2_multiple form-control', 'multiple']) !!}
	</div>
    @if ($errors->has('roles')) <p class="help-block">{{ $errors->first('roles') }}</p> @endif
</div>

<!-- Permissions -->
@if(isset($user))
    @include('shared._permissions', ['closed' => 'true', 'model' => $user ])
@endif