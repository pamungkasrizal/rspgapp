@extends('layouts.app')

@section('title', 'Create Users')
@section('breadcrumb', 'Create')
@section('breadcrumbSmall', 'Users')

@section('content')
	
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Users</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<!-- CODE HERE -->
					{!! Form::open(['route' => ['users.store'], 'class' => 'form-horizontal form-label-left' ]) !!}
						@include('user._form')
						<!-- Submit Form Button -->
						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-9 text-right">
								<a class="btn btn-danger" href="{{ route('users.index') }}">Cancel</a>
								{!! Form::submit('Create', ['class' => 'btn btn-success']) !!}
							</div>
						</div>
					{!! Form::close() !!}
					
				</div>
			</div>
		</div>
	</div>
@endsection