@extends('layouts.app')

@section('title', 'Users')
@section('breadcrumb', 'Users')
@section('breadcrumbSmall', '')

@section('content')
	
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>List Data</h2>
					<ul class="nav navbar-right panel_toolbox">
						@can('add_users')
						<li>
							<a href="{{ route('users.create') }}" style="color: #1ABB9C;font-weight: bold;">
								<i class="fa fa-plus-square"></i> tambah user
							</a>
						</li>
						<li><a style="cursor: default;">|</a></li>
						@endcan
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<!-- CODE HERE -->
					<table class="table table-striped table-bordered table-hover" id="dTUser">
						<thead>
							<tr>
								<th>Nama</th>
								<th>NIP</th>
								<th>Jabatan</th>
								<th>Status</th>
								@can('edit_users', 'delete_users')
								<th class="text-center">Actions</th>
								@endcan
							</tr>
						</thead>
					</table>
					
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer_scripts')
<script>
	$(document).ready(function () {
		
        $('#dTUser').DataTable({
            "processing": true,
            "serverSide": true,
			"pageLength": 10,
            "ajax":{
                     "url": "{{ route('getDataUser') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "name" },
                { "data": "nip" },
                { "data": "json_data.jabatan" },
				{ "data": "active",
					"render":function(data, type, full, meta){
						$arr = ['Tidak Aktif', 'Aktif'];
					   return $arr[full.active];
					}
				},
                { "data": "id",
					"render":function(data, type, full, meta){
						var $x = full.id;
					   return '@include("shared._actions", ["entity" => "users", "id" => 3 ])';
					}
				}
            ]
        });
    });
</script>
@endsection