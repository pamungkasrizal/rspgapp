<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title') Tittel | Create Account</title>

	<!-- Bootstrap-->
	<link href="{{ asset('assets/vendors-gentelella/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="{{ asset('assets/vendors-gentelella/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
	<!-- NProgress -->
	<link href="{{ asset('assets/vendors-gentelella/nprogress/nprogress.css') }}" rel="stylesheet">
	<!-- Animate.css -->
	<link href="{{ asset('assets/vendors-gentelella/animate.css/animate.min.css') }}" rel="stylesheet">
	<!-- iCheck -->
	<link href="{{ asset('assets/vendors-gentelella/iCheck/skins/flat/green.css') }}" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="{{ asset('assets/vendors-gentelella/build/css/custom.min.css') }}" rel="stylesheet">
</head>

<body class="login">
	<div>
		<a class="hiddenanchor" id="signup"></a>
		<a class="hiddenanchor" id="signin"></a>

		<div class="login_wrapper">

			<div id="register" class="animate form "><!-- add class : registration_form -->
				<section class="login_content">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
						{{ csrf_field() }}
					
						<h1>Create Account</h1>
						<div id="flash-msg">
							@include('flash::message')
						</div>
                        <div class="{{ $errors->has('nip') ? ' has-error' : '' }}">
							<input id="nip" type="text" class="form-control" name="nip" value="{{ old('nip') }}" placeholder="NIP atau NPP" required maxlength="18">

							@if ($errors->has('nip'))
								<span class="help-block" style="margin-top:-15px;">
									<strong style="float: left;margin-bottom: 10px;">{{ $errors->first('nip') }}</strong>
								</span>
							@endif
                        </div>
						
						<!-- Direktorat -->
                        <div class="{{ $errors->has('id_direktorat') ? ' has-error' : '' }}">
							<select id="id_direktorat" name="id_direktorat" class="select2_group form-control">
								{!! Helper::selectBoxOption($direktorat, '-- Pilih Direktorat --', null, null, false, 'Direktorat', 'Direktur') !!}
							</select>
							@if ($errors->has('id_direktorat'))
								<span class="help-block" style="margin-top:-15px;">
									<strong style="float: left;margin-bottom: 10px;">{{ $errors->first('id_direktorat') }}</strong>
								</span>
							@endif
                        </div>
						
						<!-- Bidang/Bagian -->
						<div id="divBagian">
							<select id="id_bagian" name="id_bagian" class="select2_group form-control" disabled>
								<option value="" selected="" disabled="">-- Pilih Bidang/Bagian --</option>
								<!-- append here -->
							</select>
                        </div>
						
						<!-- Seksi/Subag -->
						<div id="divSubagSeksi">
							<select id="id_subagseksi" name="id_subagseksi" class="select2_group form-control" disabled>
								<option value="" selected="" disabled="">-- Pilih Seksi/Subag --</option>
								<!-- append here -->
							</select>
                        </div>
						
						<!-- Instalasi/Ruangan -->
						<div id="divInstalasi">
							<select id="id_instalasi" name="id_instalasi" class="select2_group form-control" disabled>
								<option value="" selected="" disabled="">-- Pilih Instalasi/Ruangan --</option>
								<!-- append here -->
							</select>
                        </div>

                        <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
							<input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

							@if ($errors->has('password'))
								<span class="help-block" style="margin-top:-15px;">
									<strong style="float: left;margin-bottom: 10px;">{{ $errors->first('password') }}</strong>
								</span>
							@endif
                        </div>

                        <div class="">
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                        </div>
						
						<div>
							<button type="submit" class="btn btn-default submit" style="width: 100%;float: left;margin: 10px 15px 0 0;">
								Submit
							</button>
						</div>

						<div class="clearfix"></div>

						<div class="separator">
							<p class="change_link">Sudah Melakukan Registrasi ?
								<a href="{{ route('login') }}" class="to_register"> Log in </a>
							</p>

							<div class="clearfix"></div>
							<br />

							<div>
								<h1><i class="fa fa-stethoscope"></i> RSPGapp</h1>
								<p>©2017 November, All Rights Reserved. </br>RSPGapp is a Bootstrap 3 template. Privacy and Terms</p>
							</div>
						</div>
					</form>
				</section>
			</div>
		</div>
	</div>
	
	<!-- iCheck -->
    <script src="{{ asset('assets/vendors-gentelella/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/iCheck/icheck.min.js') }}"></script>
	<script>
	$(function($) {
		
		//change direktorat
		$('select[name="id_direktorat"]').on("change", function(e) { 
			$(this).parent().removeClass('has-error');
			$(this).next('span').remove();
			
			if($(this).val().length > 1){
				$('#id_bagian').attr('disabled', true);
				$('#id_subagseksi').attr('disabled', true);
				$('#id_instalasi').attr('disabled', true);
				$('#id_bagian').html('<option value="" selected="" disabled="">-- Pilih Bidang/Bagian --</option>');
				$('#id_subagseksi').html('<option value="" selected="" disabled="">-- Pilih Seksi/Subag --</option>');
				$('#id_instalasi').html('<option value="" selected="" disabled="">-- Pilih Instalasi/Ruangan --</option>');
				return false;
			}
			
			var $term = $(this).val(),
				$token = '{{ csrf_token() }}';
			$.ajax({
				dataType: 'html',
				url: '{{ route("getBagian") }}',
				data: { '_token': $token, 'term': $term },
				success: function (response) {
					$('#id_subagseksi').attr('disabled', true);
					$('#id_instalasi').attr('disabled', true);
					
					$("#id_bagian").html('');
					$("#id_bagian").append(response);
					$('#id_bagian').attr('disabled', false);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.responseText);
				}
			});
		});
		
		//change bidang/bagian =============================================================
		$('select[name="id_bagian"]').on("change", function(e) {
			if($(this).val().length > 1){
				$('#id_subagseksi').attr('disabled', true);
				$('#id_instalasi').attr('disabled', true);
				$('#id_subagseksi').html('<option value="" selected="" disabled="">-- Pilih Seksi/Subag --</option>');
				$('#id_instalasi').html('<option value="" selected="" disabled="">-- Pilih Instalasi/Ruangan --</option>');
				return false;
			}
			
			var $term = $(this).val(),
				$direk = $('select[name=id_direktorat]').val(),
				$token = '{{ csrf_token() }}';
			$.ajax({
				dataType: 'html',
				url: '{{ route("getSubagSeksi") }}',
				data: { '_token': $token, 'term': $term, 'direk': $direk },
				success: function (response) {
					$('#id_subagseksi').attr('disabled', true);
					$('#id_instalasi').attr('disabled', true);
					
					$("#id_subagseksi").html('');
					$("#id_subagseksi").append(response);
					$('#id_subagseksi').attr('disabled', false);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.responseText);
				}
			});
		});
		
		//change subag/seksi =============================================================
		$('select[name="id_subagseksi"]').on("change", function(e) {
			if($(this).val().length > 2){
				$('#id_instalasi').attr('disabled', true);
				$('option#id_instalasi[value=""]').attr('selected',true);
				return false;
			}
			
			var $term = $(this).val(),
				$direk = $('select[name=id_direktorat]').val(),
				$bagian = $('select[name=id_bagian]').val(),
				$token = '{{ csrf_token() }}';
			$.ajax({
				dataType: 'html',
				url: '{{ route("getInstalasi") }}',
				data: { '_token': $token, 'term': $term, 'direk': $direk, 'bagian': $bagian },
				success: function (response) {					
					$("#id_instalasi").html('');
					$("#id_instalasi").append(response);
					$('#id_instalasi').attr('disabled', false);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.responseText);
				}
			});
		});
		
	});
	</script>
</body>
</html>