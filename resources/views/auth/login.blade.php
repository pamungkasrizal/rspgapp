<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Login | RSPGapp</title>
	<link rel="shortcut icon" href="">
	<!-- Bootstrap-->
	<link href="{{ asset('assets/vendors-gentelella/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="{{ asset('assets/vendors-gentelella/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
	<!-- NProgress -->
	<link href="{{ asset('assets/vendors-gentelella/nprogress/nprogress.css') }}" rel="stylesheet">
	<!-- Animate.css -->
	<link href="{{ asset('assets/vendors-gentelella/animate.css/animate.min.css') }}" rel="stylesheet">
	<!-- iCheck -->
	<link href="{{ asset('assets/vendors-gentelella/iCheck/skins/flat/green.css') }}" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="{{ asset('assets/vendors-gentelella/build/css/custom.min.css') }}" rel="stylesheet">
</head>

<body class="login">
	<div>
		<a class="hiddenanchor" id="signup"></a>
		<a class="hiddenanchor" id="signin"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
						{{ csrf_field() }}
					
						<h1>Login Form</h1>
						<div class="{{ $errors->has('nip') ? ' has-error' : '' }}">
							<input id="nip" type="text" class="form-control" placeholder="NIP atau NPP" name="nip" value="{{ old('nip') }}" required maxlength="18"/>
							@if ($errors->has('nip'))
								<span class="help-block" style="margin-top: -15px;">
									<strong style="float: left;margin-bottom: 10px;">{{ $errors->first('nip') }}</strong>
								</span>
							@endif
						</div>
						<div class="{{ $errors->has('password') ? ' has-error' : '' }}" style="margin-bottom:-10px;">
							<input id="password" type="password" class="form-control" name="password" placeholder="Password" required value="{{ old('password') }}">
							@if ($errors->has('password'))
								<span class="help-block" style="margin-top:-15px;float: left;width: 100%;text-align: left;">
									<strong style="margin-bottom: 10px;">{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</div>
						<div class="checkbox">
							<label style="float: left;">
								<input type="checkbox" class="flat" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
							</label>
						</div>
						<div>
							<button type="submit" class="btn btn-default submit" style="width: 100%;float: left;margin: 10px 15px 0 0;">
								Login
							</button>
							<!--<a class="reset_pass" href="{{ route('password.request') }}" style="margin-right: 0;">
								Forgot Your Password?
							</a>-->
						</div>

						<div class="clearfix"></div>

						<div class="separator">
							<p class="change_link">Belum Aktif/Terdaftar?
								<a href="{{ route('register') }}" class="to_register"> Registrasi</a>

							<div class="clearfix"></div>
							<br />

							<div>
								<h1><i class="fa fa-stethoscope"></i> RSPGapp</h1>
								<p>©2017 November, All Rights Reserved. </br>RSPGapp is a Bootstrap 3 template. Privacy and Terms</p>
							</div>
						</div>
					</form>
				</section>
			</div>
			
		</div>
	</div>
	
	<!-- iCheck -->
    <script src="{{ asset('assets/vendors-gentelella/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendors-gentelella/iCheck/icheck.min.js') }}"></script>
</body>
</html>