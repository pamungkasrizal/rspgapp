@extends('layouts.app')

@section('title', 'e-Prescribings Dashboard')
@section('breadcrumb', 'Dashboard e-Prescribings')
@section('breadcrumbSmall', '')

@section('content')
	
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>List Data <small>Order Pasien</small></h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Settings 1</a></li>
								<li><a href="#">Settings 2</a></li>
							</ul>
						</li>
						<li><a class="close-link"><i class="fa fa-close"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="table-responsive">
						<table class="table table-striped jambo_table bulk_action" id="datatable">
							<thead>
								<tr class="headings">
									<th class="column-title">No</th>
									<th class="column-title">Tgl Reg</th>
									<th class="column-title">Noreg</th>
									<th class="column-title">Nama Pasien</th>
									<th class="column-title">Umur</th>
									<th class="column-title">Jml</th>
									<th class="column-title">Department</th>
									<th class="column-title">Dokter</th>
									<th class="column-title no-link last"><span class="nobr">Action</span></th>
								</tr>
							</thead>
							<tbody>
								@forelse($data as $key=>$val)
									<tr class="even pointer">
										<td>{{ ++$key }}</td>
										<td>{{ date('d/m/Y', strtotime($val->tgl_reg)) }}</td>
										<td>{{ trim($val->no_reg) }}</td>
										<td>{{ trim($val->nama_pas) }}</td>
										<td>{{ trim($val->umur) }}</td>
										<td>34</td>
										<td>{{ trim($val->ds_dep) }}</td>
										<td>{{ trim($val->ds_dr) }}</td>
										<td class="last">
											<a href="{{ route('orderEprescribing', trim($val->no_reg)) }}" style="color: #1ABB9C;font-weight: bold;" title="Order">
												<i class="fa fa-shopping-cart"></i> order
											</a> | 
											<a href="{{ route('historyEprescribing', trim($val->no_mr)).'/'.trim($val->no_reg) }}" style="color: #1ABB9C;font-weight: bold;" title="History">
												<i class="fa fa-history"></i> history
											</a>
										</td>
									</tr>
								@empty
									<tr>
										<td colspan="9" class="text-center">Data is empty !!</td>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>						
				</div>
			</div>
		</div>
	</div>
@endsection