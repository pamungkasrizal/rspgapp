@extends('layouts.app')

@section('title', 'e-Prescribings History')
@section('breadcrumb', 'History e-Prescribings')
@section('breadcrumbSmall', '')

@section('content')
	
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>List Data <small>History Pasien</small></h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Settings 1</a></li>
								<li><a href="#">Settings 2</a></li>
							</ul>
						</li>
						<li><a class="close-link"><i class="fa fa-close"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="table-responsive">
						<div class="col-md-4 hidden-small">
                          <table class="countries_list">
                            <tbody>
                              <tr>
                                <td>No MR</td>
                                <td class="fs15 fw700 text-right"><b>{{ $dataPasien->no_mr }}</b></td>
                              </tr>
                              <tr>
                                <td>Nama</td>
                                <td class="fs15 fw700 text-right"><b>{{ $dataPasien->nama_pas }}</b></td>
                              </tr>
                              <tr>
                                <td>Umur</td>
                                <td class="fs15 fw700 text-right"><b>{{ $dataPasien->umur }}</b></td>
                              </tr>
							  <tr>
                                <td>Jenis Kelamin</td>
                                <td class="fs15 fw700 text-right"><b>{{ ($dataPasien->lp == 'L') ? 'Laki-laki' : 'Perempuan' }}</b></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
						<table class="table table-striped jambo_table bulk_action">
							<thead>
								<tr class="headings">
									<th class="column-title">No</th>
									<th class="column-title">Noreg</th>
									<th class="column-title">Tanggal Order</th>
									<th class="column-title">No Order</th>
									<th class="column-title">Department</th>
									<th class="column-title">Dokter</th>
									<th class="column-title">Dokter DPJP</th>
									<th class="column-title">Tipe Resep</th>
									<th class="column-title">Approved</th>
									<th class="column-title no-link last"><span class="nobr">Action</span></th>
								</tr>
							</thead>
							<tbody>
							@if(count($data) > 0)
								@forelse($data as $key=>$val)
								<tr class="even pointer">
									<td>{{ $key+1 }}</td>
									<td>{{ $val->no_reg }}</td>
									<td>{{ date('d/m/Y', strtotime($val->tgl_order)) }}</td>
									<td>{{ $val->no_order }}</td>
									<td>{{ $val->ds_dep }}</td>
									<td>{{ $val->ds_dr }}</td>
									<td>{{ $val->ds_dr }}</td>
									<td>Dr. Sulaiman</td>
									<td>-</td>
									<td class="last">
										<a href="{{ route('orderEprescribing', trim($no_reg)).'/'.trim($val->no_order) }}" style="color: #1ABB9C;font-weight: bold;" title="Order">
											<i class="fa fa-reply-all"></i> order ulang
										</a> | 
										<a href="#" style="color: #1ABB9C;font-weight: bold;" title="History">
											<i class="fa fa-print"></i> print/view
										</a>
									</td>
								</tr>
								@empty
									<tr>
										<td colspan="10" class="text-center">Data is empty !!</td>
									</tr>
								@endforelse
							@endif
							</tbody>
						</table>
					</div>						
				</div>
			</div>
		</div>
	</div>
@endsection