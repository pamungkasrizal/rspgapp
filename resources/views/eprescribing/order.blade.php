@extends('layouts.app')

@section('title', 'e-Prescribings Order')
@section('breadcrumb', 'Order e-Prescribings')
@section('breadcrumbSmall', '')

@section('content')
	
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>List Data <small>Order Pasien</small></h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a style="cursor: default; left:33px;">|</a></li>
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="table-responsive" style="overflow:  hidden;">
						<div class="col-md-3 hidden-small">
                          <table class="countries_list">
                            <tbody>
                              <tr>
                                <td>No MR</td>
                                <td class="fs15 fw700 text-right"><b>{{ $data->no_mr }}</b></td>
                              </tr>
                              <tr>
                                <td>Nama</td>
                                <td class="fs15 fw700 text-right"><b>{{ $data->nama_pas }}</b></td>
                              </tr>
                              <tr>
                                <td>Umur</td>
                                <td class="fs15 fw700 text-right"><b>{{ $data->umur }}</b></td>
                              </tr>
							  <tr>
                                <td>Jenis Kelamin</td>
                                <td class="fs15 fw700 text-right"><b>{{ ($data->lp == 'L') ? 'Laki-laki' : 'Perempuan' }}</b></td>
                              </tr>
							  <tr>
                                <td>Jaminan</td>
                                <td class="fs15 fw700 text-right"><b>{{ (trim($data->kd_perush) == '') ? 'Tunai' : $data->kd_perush }}</b></td>
                              </tr>
                              <tr>
                                <td>ICD</td>
                                <td class="fs15 fw700 text-right"><b>{{ $data->ds_icd_masuk }}</b></td>
                              </tr>
                              <tr>
                                <td>Dokter</td>
                                <td class="fs15 fw700 text-right"><b>{{ $data->ds_dr }}</b></td>
                              </tr>
							  <tr>
                                <td>Dokter DPJP</td>
                                <td class="fs15 fw700 text-right"><b>{{ '-' }}</b></td>
                              </tr>
							  <tr>
                                <td>Department</td>
                                <td class="fs15 fw700 text-right"><b>{{ $data->ds_dep }}</b></td>
                              </tr>
                              <tr>
                                <td>Alergi</td>
                                <td class="fs15 fw700 text-right"><b>{{ '-' }}</b></td>
                              </tr>
                              <tr>
                                <td>Berat Badan</td>
                                <td class="fs15 fw700 text-right"><b>{{ $data->berat_badan }}</b></td>
                              </tr>
							  <tr>
                                <td>Tinggi Badan</td>
                                <td class="fs15 fw700 text-right"><b>{{ $data->tinggi_badan }}</b></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
						<div class="col-md-9 hidden-small">
							<div class="x_panel">
								{!! Form::open(['id' => 'orderFarmasi', 'class' => 'form-horizontal form-label-left' ]) !!}
								<div class="x_title">
									<h2>Order Farmasi <small></small></h2>
									<ul class="nav navbar-right panel_toolbox">
										<li><a style="cursor: default; left:33px;">|</a></li>
										<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<div class="form-group">
										<div class="col-md-3 col-sm-3 text-center">
											<div class="checkbox">
												<label>
													{!! Form::radio('racikan', 0, true, ['class' => 'flat']) !!} Non Racikan
												</label>
											</div>
										</div>
										<div class="col-md-4 col-sm-4">
											<div class="checkbox">
												<label>
													{!! Form::radio('racikan', 1, false, ['class' => 'flat']) !!} Racikan
												</label>
												<label class="racikanHideShow" style="display:none;">
													( {!! Form::checkbox('dtd', 1, true, ['class' => 'flat']) !!} DTD )
												</label>
											</div>
										</div>
									</div>
									<div class="form-group">{!! Form::hidden('signaID', '', ['id' => 'signaID']) !!}</div>
									<div class="form-group clone">
										<div class="col-md-6 col-sm-6">
											<div class="col-md-12 col-sm-12 col-xs-12 form-group">
												{!! Form::text('nama_barang[]', null, ['id' => 'nama_barang', 'class' => 'form-control col-md-10 col-xs-12 nama_barang', 'placeholder' => 'Nama Obat / Alkes', 'autocomplete' => 'off', 'onfocus' => 'createTypeahead(this.id)']) !!}
												
												{!! Form::hidden('kode_barang[]', null, ['id' => 'kode_barang', 'class' => 'kode_barang']) !!}
											</div>
										</div>
										<div class="col-md-5 col-sm-5">
											<div class="col-md-3 col-sm-3 col-xs-12 form-group racikanHideShow" style="display:none;">
												{!! Form::text('dosis[]', null, ['id' => 'dosis', 'class' => 'form-control dosis autoNumeric', 'maxlength' => '5', 'placeholder' => 'Dosis', 'data-a-sep' => '', 'data-v-min' => '0', 'data-v-max' => '9999.99', 'data-a-pad' => 'false']) !!}
											</div>
											<div class="col-md-6 col-sm-6 col-xs-12 form-group racikanHideShow" style="display:none;">
												{!! Form::select('kd_satuan_etiket[]', $satuan, 10, ['class' => 'form-control kd_satuan_etiket', 'id' => 'kd_satuan_etiket']) !!}
											</div>
											<div class="col-md-3 col-sm-3 col-xs-12 form-group">
												{!! Form::text('qty[]', 0, ['id' => 'qty', 'class' => 'form-control qty', 'maxlength' => '5', 'placeholder' => 'Qty']) !!}
											</div>
										</div>
									</div>
									<div class="form-group text-center racikanHideShow" style="display:none;">
										<a href="#" class="add" rel=".clone" style="color: #1ABB9C;" title="Add New Item"><i class="fa fa-plus"></i> add new item</a>
									</div>
									<div class="form-group">
										<label class="col-md-9 col-sm-9 col-xs-12">
											<a href="javascript:void(0)" id="signa">
												<h4>Form Signa <i class="fa fa-medkit"></i></h4> 
											</a>
										</label>
									</div>
									<div class="form-group signa" style="display:none;">
										<div class="col-md-6 col-sm-6">
											<label class="col-md-4 col-sm-4 col-xs-12">Signa </label>
											<div class="col-md-8 col-sm-8 col-xs-12">
												{!! Form::select('signa_name', ['(1x1) Sekali sehari' => '(1x1) Sekali sehari', '(2x1) Dua kali sehari' => '(2x1) Dua kali sehari', '(3x1) Tiga kali sehari' => '(3x1) Tiga kali sehari', '(4x1) Empat kali sehari' => '(4x1) Empat kali sehari'], null, ['class' => 'form-control']) !!}
											</div>
										</div>
										<div class="col-md-6 col-sm-6">
											<label class="col-md-4 col-sm-4 col-xs-12">Lokasi penggunaan </label>
											<div class="col-md-8 col-sm-8 col-xs-12">
												{!! Form::select('kd_ket', $lokasiPg, 00, ['class' => 'form-control']) !!}
											</div>
										</div>
									</div>
									<div class="form-group signa" style="display:none;">
										<div class="col-md-6 col-sm-6">
											<label class="col-md-4 col-sm-4 col-xs-12">Waktu Makan </label>
											<div class="col-md-8 col-sm-8 col-xs-12">
												{!! Form::select('kd_jam_makan', $jamMakan, 2, ['class' => 'form-control']) !!}
											</div>
										</div>
										<div class="col-md-6 col-sm-6 racikanHideShow" style="display:none;">
											<label class="col-md-4 col-sm-4 col-xs-12">Jumlah Racikan</label>
											<div class="col-md-3 col-sm-3 col-xs-12">
												{!! Form::text('qty_racikan', 0, ['id' => 'qty_racikan', 'class' => 'form-control', 'maxlength' => '4', 'placeholder' => 'Qty']) !!}
											</div>
										</div>
									</div>
									<div class="form-group signa" style="display:none;">
										<div class="col-md-12 col-sm-12">
											<label class="col-md-2 col-sm-2 col-xs-12">Signa Tambahan / Catatan </label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												{!! Form::textarea('note', '-', ['class' => 'form-control', 'style' => 'margin-left: -4px;', 'rows' => '2']) !!}
											</div>
										</div>
									</div>
									<div class="form-group signa" style="display:none;">
										<div class="col-md-12 col-sm-12">
											<label class="col-md-2 col-sm-2 col-xs-12"> </label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												{!! Form::button('Tambah', ['id' => 'add', 'class' => 'btn btn-success', 'style' => 'margin-left: -4px;']) !!}
												{!! Form::button('Batal', ['onclick' => 'refreshInput()', 'class' => 'btn btn-danger', 'style' => 'margin-left: -4px;']) !!}
											</div>
										</div>
									</div>
								</div>
								{!! Form::close() !!}
							</div>
							
							{!! Form::open() !!}
							<table class="table table-striped jambo_table bulk_action" id="signaTable">
								<thead>
									<tr class="headings">
										<th class="column-title">Kode Signa</th>
										<th class="column-title">Nama Item</th>
										<th class="column-title">Qty</th>
										<th class="column-title" width="25%">Signa</th>
										<th class="column-title no-link last"><span class="nobr">Action</span></th>
									</tr>
								</thead>
								<tbody>
								
								</tbody>
							</table>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success countTr" style="display:none;" name="submit" value="in">Kirim Rincian Order Ke Farmasi RS</button>
								<button type="submit" class="btn btn-primary countTr" style="display:none;" name="submit" value="out">Cetak Order Ke Luar RS</button>
								<input type="hidden" value="{{ $data->no_mr }}" name="no_mr" />
							</div>
							{!! Form::close() !!}
                        </div>
					</div>						
				</div>
			</div>
		</div>
	</div>
@endsection

@section('header_styles')
<style>
	.typeahead>li>a {
		font-size: larger !important;
	}
	ul.typeahead {
		width: 95.5% !important;
	}
</style>
@endsection

@section('footer_scripts')
<script>
	var $racikan = $no = $count = 0,
		$dtd = 1,
		$checkEmpty = true,
		$no_order = 'R'+moment().format('YYMMDHHmmSS');
			
	$(document).ready(function(){
		//order e-prescribing
		$('#orderFarmasi #nama_barang').focus();
			
		$('#signa').on('click', function(){
			$('#orderFarmasi #nama_barang').focus();
			$(".signa").slideDown("slow");
		});
		
		createTypeahead('nama_barang');
		
		var removeLink = '<a class="remove" href="#" onclick="$(this).parent().slideUp(function(){ $(this).remove() }); return false" style="color: #a94442;" title="Delete"><i class="fa fa-close"></i> remove</a>';
		$('a.add').relCopy({ append: removeLink, limit: 5});
		
		$('input[name=racikan]').on('ifChecked', function(event){
			$racikan = $(this).val();
			if($racikan == 1){
				$('.racikanHideShow').show();
				$('.qty').attr('readonly', true);
			}else{
				$('.racikanHideShow').hide();
				$('.qty').attr('readonly', false);
				$('a.remove').click();
			}
			
			refreshInput();
		});
		
		$('input[name=dtd]').on('ifChecked', function(event){
			$dtd = 1;
			eachDtd();
		}).on('ifUnchecked', function(event){
			$dtd = 0;
			eachDtd();
		});
		
		$('#orderFarmasi #dosis*').on('keyup', function(){
			countDtd($(this));
		});
		
		//$('.autoNumeric').autoNumeric('init');
		
		$('#qty_racikan').on('keyup', function(){
			eachDtd();
		});
		
		$('#add').on('click', function(){
			$checkEmpty = true;
				
			var $signa_id = ($('#signaID').val() !== '') ? $('#signaID').val() : Math.floor(1000 + Math.random() * 9999),
				$signa_name = $('select[name=signa_name] option:selected').text(),
				$kd_jam_makan = $('select[name=kd_jam_makan] option:selected').val(),
				$name_jam_makan = $('select[name=kd_jam_makan] option:selected').text(),
				$kd_ket = $('select[name=kd_ket] option:selected').val(),
				$name_ket = $('select[name=kd_ket] option:selected').text(),
				$note = $('textarea[name=note]').val(),
				$html = $hiddenForm = '';
				
			$('.clone').each(function($i){
				var $nama_barang = $(this).find('.nama_barang').val(),
					$kode_barang = $(this).find('.kode_barang').val(),
					$dosis = $(this).find('.dosis').val(),
					$kd_satuan_etiket = $(this).find('.kd_satuan_etiket option:selected').val(),
					$name_satuan_etiket = $(this).find('.kd_satuan_etiket option:selected').text(),
					$qty = $(this).find('.qty').val();
					
					if(!$nama_barang || !kode_barang){
						$('#nama_barang').parent().addClass('has-error');
						$checkEmpty = false;
					}else{
						$('#nama_barang').parent().removeClass('has-error');
					}
					
					if($racikan == 1 && (!$dosis || $dosis < 1)){
						$('#dosis').parent().addClass('has-error');
						$checkEmpty = false;
					}else{
						$('#dosis').parent().removeClass('has-error');
					}
					
					if($racikan == 1 && !$kd_satuan_etiket){
						$('#kd_satuan_etiket').parent().addClass('has-error');
						$checkEmpty = false;
					}else{
						$('#kd_satuan_etiket').parent().removeClass('has-error');
					}
					
					if(!$qty || $qty < 1){
						$('#qty').parent().addClass('has-error');
						$checkEmpty = false;
					}else{
						$('#qty').parent().removeClass('has-error');
					}
					
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][no_mr]" value="{{ trim($data->no_mr) }}">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][no_reg]" value="{{ trim($data->no_reg) }}">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][nama_pas]" value="{{ trim($data->nama_pas) }}">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][kd_dr]" value="{{ trim($data->kd_dr) }}">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][ds_dr]" value="{{ trim($data->ds_dr) }}">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][kd_dep]" value="{{ trim($data->kd_dep) }}">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][ds_dep]" value="{{ trim($data->ds_dep) }}">',
					
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][no_order]" value="'+$no_order+'">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][signa_id]" value="'+$signa_id+'">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][racikan]" value="'+$racikan+'">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][dtd]" value="'+$dtd+'">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][kode_barang]" value="'+$kode_barang+'">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][nama_barang]" value="'+$nama_barang+'">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][dosis]" value="'+$dosis+'">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][kd_satuan_etiket]" value="'+$kd_satuan_etiket+'">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][name_satuan_etiket]" value="'+$name_satuan_etiket+'">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][qty]" value="'+$qty+'">',
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][qty_racikan]" value="'+$('#qty_racikan').val()+'">';
					
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][signa_name]" value="'+$signa_name+'">';
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][kd_jam_makan]" value="'+$kd_jam_makan+'">';
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][name_jam_makan]" value="'+$name_jam_makan+'">';
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][kd_ket]" value="'+$kd_ket+'">';
					$hiddenForm += '<input type="hidden" name="rows['+$no+'][name_ket]" value="'+$name_ket+'">';
					
					$no = $no+1;
					$qty = ($racikan == 1) ? ' : '+$qty : $qty;
					$name_satuan_etiket = ($racikan == 1) ? $name_satuan_etiket : '';
					$racikanBold = ($racikan == 1) ? '<b>Racikan.</b>' : '';
					
					$html += '<tr class="even pointer tr'+$signa_id+'"><td>'+$racikanBold+$signa_id+'</td>';
					$html += '<td>'+$nama_barang+'</td><td>'+$dosis+' '+$name_satuan_etiket+$qty+'</td>';
					$html += '<td class="bLast">'+$signa_name.split(' ')[0]+' : '+$name_jam_makan+' : '+$name_ket+' : '+ $note + $hiddenForm+'</td>';
					if($i == 0) $html += '<td class="last"><a href="javascript:void(0)" style="color: #1ABB9C;font-weight: bold;" title="Edit" onclick="editTr('+$racikan+', \'tr'+$signa_id+'\')"><i class="fa fa-edit"></i> edit</a> | <a href="javascript:void(0)" style="color: #1ABB9C;font-weight: bold;" title="Hapus" onclick="$(\'.tr'+$signa_id+'\').slideUp(function(){ $(this).remove(); countTr(); }); return false"><i class="fa fa-trash"></i> hapus</a></td></tr>';
					$hiddenForm = '';
			});
			
			if($racikan == 1){
				var $valq = $('#qty_racikan').val();
				if($valq == '' || $valq == 0){
					$('#qty_racikan').parent().addClass('has-error');
					return false;
				}
			}
			
			if($checkEmpty){
				if($('#signaID').val() !== ''){
					$('tr.tr'+$('#signaID').val()).remove();
				}
				$('#signaTable > tbody:last-child').append($html);
				refreshInput();
			}
			
		});
		
		@if($dataEpresc !== null)
			var $nox = $x = 0,
				$html = $hiddenForm = '';
				
			@foreach ($dataEpresc as $dataVal)
			
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][no_mr]" value="{{ trim($data->no_mr) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][no_reg]" value="{{ trim($data->no_reg) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][nama_pas]" value="{{ trim($data->nama_pas) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][kd_dr]" value="{{ trim($data->kd_dr) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][ds_dr]" value="{{ trim($data->ds_dr) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][kd_dep]" value="{{ trim($data->kd_dep) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][ds_dep]" value="{{ trim($data->ds_dep) }}">',
				
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][no_order]" value="'+$no_order+'">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][signa_id]" value="{{ trim($dataVal->signa_id) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][racikan]" value="{{ trim($dataVal->racikan) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][dtd]" value="{{ trim($dataVal->dtd) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][kode_barang]" value="{{ trim($dataVal->kode_barang) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][nama_barang]" value="{{ trim($dataVal->nama_barang) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][dosis]" value="{{ trim($dataVal->dosis) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][kd_satuan_etiket]" value="{{ trim($dataVal->kd_satuan_etiket) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][name_satuan_etiket]" value="{{ trim($dataVal->name_satuan_etiket) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][qty]" value="{{ trim($dataVal->qty) }}">',
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][qty_racikan]" value="{{ trim($dataVal->qty_racikan) }}">';
				
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][signa_name]" value="{{ trim($dataVal->signa_name) }}">';
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][kd_jam_makan]" value="{{ trim($dataVal->kd_jam_makan) }}">';
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][name_jam_makan]" value="{{ trim($dataVal->name_jam_makan) }}">';
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][kd_ket]" value="{{ trim($dataVal->kd_ket) }}">';
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][name_ket]" value="{{ trim($dataVal->name_ket) }}">';
				$hiddenForm += '<input type="hidden" name="rows['+$nox+'][note]" value="{{ trim($dataVal->note) }}">';
				
				$nox = $nox+1;
				var $qty = ("{{ trim($dataVal->racikan) }}" == 1) ? '{{ trim($dataVal->name_satuan_etiket) }} : '+"{{ trim($dataVal->qty) }}" : "{{ trim($dataVal->qty) }}",
					$name_satuan_etiket = ("{{ trim($dataVal->racikan) }}" == 1) ? "{{ trim($dataVal->name_satuan_etiket) }}" : '',
					$racikanBold = ("{{ trim($dataVal->racikan) }}" == 1) ? '<b>Racikan.</b>' : '',
					$signa_name = "{{ trim($dataVal->signa_name) }}";
				
				$html += '<tr class="even pointer tr{{ trim($dataVal->signa_id) }}"><td>'+$racikanBold+'{{ trim($dataVal->signa_id) }}</td>';
				$html += '<td>{{ trim($dataVal->nama_barang) }}</td><td>{{ trim($dataVal->dosis) }} '+$qty+'</td>';
				$html += '<td class="bLast">'+$signa_name.split(' ')[0]+' : {{ trim($dataVal->name_jam_makan) }} : {{ trim($dataVal->name_ket) }} : {{ trim($dataVal->name_ket) }}'+ $hiddenForm+'</td>';
				if($x !== "{{ trim($dataVal->signa_id) }}") $html += '<td class="last"><a href="javascript:void(0)" style="color: #1ABB9C;font-weight: bold;" title="Edit" onclick="editTr({{ trim($dataVal->racikan) }}, \'tr{{ trim($dataVal->signa_id) }}\')"><i class="fa fa-edit"></i> edit</a> | <a href="javascript:void(0)" style="color: #1ABB9C;font-weight: bold;" title="Hapus" onclick="$(\'.tr{{ trim($dataVal->signa_id) }}\').slideUp(function(){ $(this).remove(); countTr(); }); return false"><i class="fa fa-trash"></i> hapus</a></td></tr>';
				$hiddenForm = '';
				$x = "{{ trim($dataVal->signa_id) }}";
					
			@endforeach
			
			$('#signaTable > tbody:last-child').append($html);
			$('.countTr').show();
		@endif
		//END order e-prescribing
	});
	
	function createTypeahead(id)
	{
		$("#"+id).typeahead({
			source: function(query, result)
			{
				$.ajax({
					url:'{{ route("getBarangFarmasi") }}',
					method:"get",
					data:{'term': query},
					dataType:"json",
					success:function(data)
					{
						result($.map(data, function(item){
							return item;
						}));
					}
				})
			},
			updater: function(obj) {
				$(".signa").slideDown("slow");
				$('.nama_barang').typeahead('destroy');
				
				return obj;
			},
			afterSelect: function (item) {
				var $id = (id).slice(-1),
					$id = ($id == 'e' || $id == 'g') ? '#kode_barang' : '#kode_barang'+$id;
				$($id).val(item.id);
				$('.nama_barang').typeahead('destroy');
			}
		});
	}
	
	function countDtd($ths)
	{
		if($racikan == 0) return false;
		
		var $id = ($ths.attr('id')).slice(-1),
			$id = ($id == 's' || $id == 'y') ? '#qty' : '#qty'+$id,
			$val = $ths.val(),
			$count = ($dtd == 1) ? $val * $('#qty_racikan').val() : $val;
			
		$($id).val($count);
	}
	
	function eachDtd()
	{
		$(".dosis").each(function(){
			countDtd($(this));
		});
	}
	
	function refreshInput()
	{
		$('a.remove').click();
		$('#nama_barang, #kode_barang, #dosis').val('');
		$('#qty').val(0);
		$('#qty_racikan').parent().removeClass('has-error');
		$('#signaID').val('');
		countTr();
	}
	
	function countTr(){
		$count = $('#signaTable >tbody >tr').length;
		($count > 0) ? $('.countTr').show() : $('.countTr').hide();
	}
	
	function editTr($r, $idTr){
		$(":radio[name=racikan][value="+$r+"]").iCheck('check');
		$('#signaID').val($('.'+$idTr).find("td.bLast input:eq(8)").val());
		
		$('.'+$idTr).each(function($n){
			$i = ($n > 0) ? $n+1 : '';
			$('input[id=kode_barang'+$i+']').val($(this).find("td.bLast input:eq(11)").val());
			$('input[id=nama_barang'+$i+']').val($(this).find("td.bLast input:eq(12)").val());
			$('input[id=dosis'+$i+']').val($(this).find("td.bLast input:eq(13)").val());
			$('input[id=qty'+$i+']').val($(this).find("td.bLast input:eq(16)").val());
			if($n !== $('.'+$idTr).length - 1 ) $('a.add').click();
		});
		
		//signa
		$('select[name=signa_name]').val($('.'+$idTr).find("td.bLast input:eq(18)").val());
		$('select[name=kd_jam_makan]').val($('.'+$idTr).find("td.bLast input:eq(19)").val());
		$('select[name=kd_ket]').val($('.'+$idTr).find("td.bLast input:eq(21)").val());
		$('input[name=qty_racikan]').val($('.'+$idTr).find("td.bLast input:eq(17)").val());
	}
</script>
@endsection