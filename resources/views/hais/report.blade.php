@extends('layouts.app')

@section('title', 'HAIs Report')
@section('breadcrumb', 'Report HAIs')
@section('breadcrumbSmall', '')

@section('content')
	
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<ul class="nav navbar-right panel_toolbox">
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<!-- CODE HERE -->
					<h2>Laporan Harian</h2>
					<br/>
					<table class="table table-striped table-bordered table-hover" id="dTReportHAIs" style="width:100% !important;">
						<thead>
							<tr>
								<th>No MR</th>
								<th>No Reg</th>
								<th>Nama</th>
								<th>Ruangan</th>
								<th>PVL</th>
								<th>CVL</th>
								<th>UC</th>
								<th>ETT</th>
								<th>WSD</th>
								<th>Tirah</th>
								<th>Operasi</th>
								
								<th>IAD</th>
								<th>VAP</th>
								<th>IDO</th>
								<th>ISK</th>
								<th>HAP</th>
								<th>Phleb</th>
								<th>Decub</th>
								<th>inf WSD</th>
							</tr>
						</thead>
					</table>
					
					<br/><br/>
					<h2>Laporan <small><i>Per</i></small> Ruangan <small>Tahun</small> {{ date('Y') }}</h2>
					<br/>
					<table class="table table-striped table-bordered table-hover" id="dTReportHAIs" style="width:100% !important;">
						<thead>
							<tr>
								<th>Ruangan</th>
								<th>PVL</th>
								<th>CVL</th>
								<th>UC</th>
								<th>ETT</th>
								<th>WSD</th>
								<th>Tirah</th>
								<th>Operasi</th>
								
								<th>IAD</th>
								<th>VAP</th>
								<th>IDO</th>
								<th>ISK</th>
								<th>HAP</th>
								<th>Phleb</th>
								<th>Decub</th>
								<th>inf WSD</th>
							</tr>
						</thead>
						<tbody>
							
							<tr>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
								<td>xxx</td>
							</tr>
							
						</tbody>
					</table>
					
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer_scripts')
<script>
	$(document).ready(function () {
		
        $('#dTReportHAIs').DataTable({
            "processing": true,
            "serverSide": true,
			"pageLength": 10,
            "ajax":{
                     "url": "{{ route('getReportHais') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "data_kunjungan.no_mr" },
                { "data": "no_reg" },
                { "data": "data_kunjungan.nama_pas" },
                { "data": "data_kunjungan.ds_dep" },
                { "data": "data_pemasangan.pvl.day", "defaultContent": "-" },
                { "data": "data_pemasangan.cvl.day", "defaultContent": "-" },
                { "data": "data_pemasangan.uv.day", "defaultContent": "-" },
                { "data": "data_pemasangan.ett.day", "defaultContent": "-" },
                { "data": "data_pemasangan.wsd.day", "defaultContent": "-" },
                { "data": "data_pemasangan.tirah_baring.day", "defaultContent": "-" },
                { "data": "data_pemasangan.post_operasi.startDate",
					"render":function(data, type, full, meta){
					   return (typeof full.data_pemasangan.post_operasi != 'undefined') ? moment(full.data_pemasangan.post_operasi.startDate).format('DD/MM/YYYY') : '-';
					}
				},
				{ "data": "data_irs.iad.date",
					"render":function(data, type, full, meta){
					   return (typeof full.data_irs.iad != 'undefined') ? moment(full.data_irs.iad.date).format('DD/MM/YYYY') : '-';
					}
				},
				{ "data": "data_irs.vap.date",
					"render":function(data, type, full, meta){
					   return (typeof full.data_irs.vap != 'undefined') ? moment(full.data_irs.vap.date).format('DD/MM/YYYY') : '-';
					}
				},
				{ "data": "data_irs.ido.date", 
					"render":function(data, type, full, meta){
					   return (typeof full.data_irs.ido != 'undefined') ? moment(full.data_irs.ido.date).format('DD/MM/YYYY') : '-';
					}
				},
				{ "data": "data_irs.sik.date",
					"render":function(data, type, full, meta){
					   return (typeof full.data_irs.sik != 'undefined') ? moment(full.data_irs.sik.date).format('DD/MM/YYYY') : '-';
					}
				},
				{ "data": "data_irs.hap.date",
					"render":function(data, type, full, meta){
					   return (typeof full.data_irs.hap != 'undefined') ? moment(full.data_irs.hap.date).format('DD/MM/YYYY') : '-';
					}
				},
				{ "data": "data_irs.phleb.date",
					"render":function(data, type, full, meta){
					   return (typeof full.data_irs.phleb != 'undefined') ? moment(full.data_irs.phleb.date).format('DD/MM/YYYY') : '-';
					}
				},
				{ "data": "data_irs.decub.date",
					"render":function(data, type, full, meta){
					   return (typeof full.data_irs.decub != 'undefined') ? moment(full.data_irs.decub.date).format('DD/MM/YYYY') : '-';
					}
				},
				{ "data": "data_irs.inf_wsd.date",
					"render":function(data, type, full, meta){
					   return (typeof full.data_irs.inf_wsd != 'undefined') ? moment(full.data_irs.inf_wsd.date).format('DD/MM/YYYY') : '-';
					}
				},
            ]
        });
    });
</script>
@endsection