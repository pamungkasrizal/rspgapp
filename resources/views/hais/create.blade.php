@extends('layouts.app')

@section('title', 'Create Surveilans')
@section('breadcrumb', 'Create')
@section('breadcrumbSmall', 'Surveilans')

@section('content')
	
	<div class="row">
		{!! Form::open(['route' => ['hais.store'], 'class' => 'form-horizontal form-label-left' ]) !!}
			{!! Form::hidden('no_reg', null) !!}
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Data Kunjungan</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<br>

					<div class="form-group">
						<div class="col-md-12 col-sm-12 col-xs-12 {{ $errors->has('txt_search') ? ' has-error' : '' }}">
							<input type="text" name="txt_search" class="form-control has-feedback-left" autocomplete="off" placeholder="Search by No MR / No Reg / Nama pasien">
							<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 form-group {{ $errors->has('data_kunjungan.no_mr') ? ' has-error' : '' }}">
						<label class="control-label">No MR</label>
						<div>
							<input type="text" name="data_kunjungan[no_mr]" class="form-control" readonly="true">
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 form-group {{ $errors->has('data_kunjungan.no_reg') ? ' has-error' : '' }}">
						<label class="control-label">No Registrasi</label>
						<div>
							<input type="text" name="data_kunjungan[no_reg]" class="form-control" readonly="true">
						</div>
					</div>
					<div class="col-md-12 col-xs-12">
						<div class="col-md-6 col-sm-6 col-xs-12 form-group" style="padding-left:0;">
							<label class="control-label">Nama</label>
							<div>
								<input type="text" name="data_kunjungan[nama_pas]" class="form-control" readonly="true">
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 form-group">
							<label class="control-label">JK</label>
							<div style="padding-top:5px;">
								<label>
									<input type="radio" class="flat" name="data_kunjungan[lp]" value="L" readonly> Laki-laki
								</label>
								<label style="padding-left:20px;">
									<input type="radio" class="flat" name="data_kunjungan[lp]" value="P" readonly> Perempuan
								</label>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 form-group">
						<label class="control-label">Tgl Lahir</label>
						<div>
							<input type="text" name="data_kunjungan[tgl_lahir]" class="form-control" readonly="true">
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 form-group">
						<label class="control-label">Tgl Masuk RS</label>
						<div>
							<input type="text" name="data_kunjungan[tgl_reg]" class="form-control" readonly="true">
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 form-group">
						<label class="control-label">Tgl Pulang</label>
						<div>
							<input type="text" name="data_kunjungan[tgl_pulang]" class="form-control" readonly="true">
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 form-group">
						<label class="control-label">Ruangan</label>
						<div>
							<input type="text" name="data_kunjungan[ds_dep]" class="form-control" readonly="true">
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 form-group">
						<label class="control-label">Diagnosa Masuk</label>
						<div>
							<input type="text" name="data_kunjungan[ds_icd_masuk]" class="form-control" readonly="true">
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 form-group">
						<label class="control-label">DPJP</label>
						<div>
							<input type="text" name="data_kunjungan[ds_dr]" class="form-control" readonly="true">
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 form-group">
						<label class="control-label">AB Pengobatan / Profilaksis</label>
						<div>
							<input type="text" name="data_kunjungan[profilaksis]" class="form-control">
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 form-group">
						<label class="control-label">Diagnosa Kerja</label>
						<div>
							<input type="text" name="data_kunjungan[ds_icd_kerja]" autocomplete="off" class="form-control icd">
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 form-group">
						<label class="control-label">Diagnosa Akhir</label>
						<div>
							<input type="text" name="data_kunjungan[ds_icd_keluar]" autocomplete="off" class="form-control icd">
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="x_panel" style="padding-bottom: 98px;">
				<div class="x_title">
					<h2>Data Medis <small>(Pemeriksaan Lab & Faktor Penyakit)</small></h2>
					<ul class="nav navbar-right panel_toolbox">
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<!-- CODE HERE -->
					<h4>Hasil Laboratorium <small>(Pemeriksaan Awal)</small></h4>
					<div class="col-md-2 col-sm-2 col-xs-12 form-group">
						<label class="control-label">HB</label>
						<div>
							<input type="text" name="data_medis[hb]" class="form-control">
						</div>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12 form-group">
						<label class="control-label">HT</label>
						<div>
							<input type="text" name="data_medis[ht]" class="form-control">
						</div>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12 form-group">
						<label class="control-label">GDS</label>
						<div>
							<input type="text" name="data_medis[gds]" class="form-control">
						</div>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12 form-group">
						<label class="control-label">Trombosit</label>
						<div>
							<input type="text" name="data_medis[trombosit]" class="form-control">
						</div>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12 form-group">
						<label class="control-label">Leukosit</label>
						<div>
							<input type="text" name="data_medis[leukosit]" class="form-control">
						</div>
					</div>
					
					<br/>
					
					<h4 style="padding-top: 80px;">Kultur <small>(Komplikasi / Infeksi Nosokomial)</small></h4>
					
					<div class="col-md-12 col-sm-12 col-xs-12 form-group">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">Sputum :</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[sputum]" value="Ada"> Ada
							</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[sputum]" value="Tidak Ada" checked> Tidak Ada
							</label>
						</div>
					</div>
					
					<div class="col-md-12 col-sm-12 col-xs-12 form-group">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">Darah :</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[darah]" value="Ada"> Ada
							</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[darah]" value="Tidak Ada" checked> Tidak Ada
							</label>
						</div>
					</div>
					
					<div class="col-md-12 col-sm-12 col-xs-12 form-group">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">Urin :</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[urin]" value="Ada"> Ada
							</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[urin]" value="Tidak Ada" checked> Tidak Ada
							</label>
						</div>
					</div>
					
					<br/>
					
					<h4 style="padding-top: 125px;">Faktor Penyakit</h4>
					<div class="col-md-12 col-sm-12 col-xs-12 form-group">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">HBs Ag :</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[hbsag]" value="Positif"> Positif
							</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[hbsag]" value="Negatif"> Negatif
							</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[hbsag]" value="Tidak diperiksa" checked> Tidak diperiksa
							</label>
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 form-group">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">Anti HCV :</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[hcv]" value="Positif"> Positif
							</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[hcv]" value="Negatif"> Negatif
							</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[hcv]" value="Tidak diperiksa" checked> Tidak diperiksa
							</label>
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 form-group">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">Anti HIV :</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[hiv]" value="Positif"> Positif
							</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[hiv]" value="Negatif"> Negatif
							</label>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<label class="control-label">
								<input type="radio" class="flat" name="data_medis[hiv]" value="Tidak diperiksa" checked> Tidak diperiksa
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Tindakan Pemasangan Alat</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content" style="margin-bottom: -13px;">					
					<table class="table">
						<thead>
							<tr>
								<th width="25%">#</th>
								<th width="19%">Pemasangan Alat</th>
								<th width="18%">Tgl Pasang</th>
								<th width="18%">Tgl Lepas</th>
								<th width="10%">Hari</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>PVL</th>
								<td class="text-center"><input type="checkbox" class="flat peal" name="data_pemasangan[pvl][status]" value="Ya"></td>
								<td><input type="text" name="data_pemasangan[pvl][startDate]" class="form-control pealHS startDate" disabled></td>
								<td><input type="text" name="data_pemasangan[pvl][endDate]" class="form-control pealHS endDate" disabled></td>
								<td><input type="text" name="data_pemasangan[pvl][day]" class="form-control" readonly></td>
							</tr>
							<tr>
								<td class="text-right">PVL (additional)</td>
								<td class="text-center"><input type="checkbox" class="flat peal" name="data_pemasangan[pvl1][status]" value="Ya"></td>
								<td><input type="text" name="data_pemasangan[pvl1][startDate]" class="form-control pealHS startDate" disabled></td>
								<td><input type="text" name="data_pemasangan[pvl1][endDate]" class="form-control pealHS endDate" disabled></td>
								<td><input type="text" name="data_pemasangan[pvl1][day]" class="form-control" readonly></td>
							</tr>
							<tr>
								<td class="text-right">PVL (additional)</td>
								<td class="text-center"><input type="checkbox" class="flat peal" name="data_pemasangan[pvl2[status]" value="Ya"></td>
								<td><input type="text" name="data_pemasangan[pvl2][startDate]" class="form-control pealHS startDate" disabled></td>
								<td><input type="text" name="data_pemasangan[pvl2][endDate]" class="form-control pealHS endDate" disabled></td>
								<td><input type="text" name="data_pemasangan[pvl2][day]" class="form-control" readonly></td>
							</tr>
							<tr>
								<th>CVL</th>
								<td class="text-center"><input type="checkbox" class="flat peal" name="data_pemasangan[cvl][status]" value="Ya"></td>
								<td><input type="text" name="data_pemasangan[cvl][startDate]" class="form-control pealHS startDate" disabled></td>
								<td><input type="text" name="data_pemasangan[cvl][endDate]" class="form-control pealHS endDate" disabled></td>
								<td><input type="text" name="data_pemasangan[cvl][day]" class="form-control" readonly></td>
							</tr>
							<tr>
								<th>UC</th>
								<td class="text-center"><input type="checkbox" class="flat peal" name="data_pemasangan[uc][status]" value="Ya"></td>
								<td><input type="text" name="data_pemasangan[uc][startDate]" class="form-control pealHS startDate" disabled></td>
								<td><input type="text" name="data_pemasangan[uc][endDate]" class="form-control pealHS endDate" disabled></td>
								<td><input type="text" name="data_pemasangan[uc][day]" class="form-control" readonly></td>
							</tr>
							<tr>
								<th>ETT / Ventilator</th>
								<td class="text-center"><input type="checkbox" class="flat peal" name="data_pemasangan[ett][status]" value="Ya"></td>
								<td><input type="text" name="data_pemasangan[ett][startDate]" class="form-control pealHS startDate" disabled></td>
								<td><input type="text" name="data_pemasangan[ett][endDate]" class="form-control pealHS endDate" disabled></td>
								<td><input type="text" name="data_pemasangan[ett][day]" class="form-control" readonly></td>
							</tr>
							<tr>
								<th>WSD</th>
								<td class="text-center"><input type="checkbox" class="flat peal" name="data_pemasangan[wsd][status]" value="Ya"></td>
								<td><input type="text" name="data_pemasangan[wsd][startDate]" class="form-control pealHS startDate" disabled></td>
								<td><input type="text" name="data_pemasangan[wsd][endDate]" class="form-control pealHS endDate" disabled></td>
								<td><input type="text" name="data_pemasangan[wsd][day]" class="form-control" readonly></td>
							</tr>
							<tr>
								<th>Tirah Baring</th>
								<td class="text-center"><input type="checkbox" class="flat" name="data_pemasangan[tirah_baring][status]" value="Ya"></td>
								<td><input type="text" name="data_pemasangan[tirah_baring][startDate]" class="form-control pealHS startDate" disabled></td>
								<td><input type="text" name="data_pemasangan[tirah_baring][endDate]" class="form-control pealHS endDate" disabled></td>
								<td><input type="text" name="data_pemasangan[tirah_baring][day]" class="form-control" readonly></td>
							</tr>
							<tr>
								<th>Post Operasi</th>
								<td class="text-center"><input type="checkbox" class="flat peal" name="data_pemasangan[post_operasi][status]" value="Ya"></td>
								<td><input type="text" name="data_pemasangan[post_operasi][startDate]" class="form-control pealHS startDate" placeholder="Tgl Operasi" disabled></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>					
				</div>
			</div>
		</div>
		
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Kejadian IRS</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li style="float:right;"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table class="table">
						<thead>
							<tr>
								<th width="25%">#</th>
								<th width="19%">Kejadian IRS</th>
								<th width="18%">Tgl Kejadian</th>
								<th width="18%"></th>
								<th width="10%"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>IAD</th>
								<td class="text-center"><input type="checkbox" class="flat keja" name="data_irs[iad][status]" value="Ya"></td>
								<td><input type="text" name="data_irs[iad][date]" class="form-control kejaHS" disabled></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>VAP</th>
								<td class="text-center"><input type="checkbox" class="flat keja" name="data_irs[vap][status]" value="Ya"></td>
								<td><input type="text" name="data_irs[vap][date]" class="form-control kejaHS" disabled></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>IDO</th>
								<td class="text-center"><input type="checkbox" class="flat keja" name="data_irs[ido][status]" value="Ya"></td>
								<td><input type="text" name="data_irs[ido][date]" class="form-control kejaHS" disabled></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>ISK</th>
								<td class="text-center"><input type="checkbox" class="flat keja" name="data_irs[isk][status]" value="Ya"></td>
								<td><input type="text" name="data_irs[isk][date]" class="form-control kejaHS" disabled></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>HAP</th>
								<td class="text-center"><input type="checkbox" class="flat keja" name="data_irs[hap][status]" value="Ya"></td>
								<td><input type="text" name="data_irs[hap][date]" class="form-control kejaHS" disabled></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>Phleb</th>
								<td class="text-center"><input type="checkbox" class="flat keja" name="data_irs[phleb][status]" value="Ya"></td>
								<td><input type="text" name="data_irs[phleb][date]" class="form-control kejaHS" disabled></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>Decub</th>
								<td class="text-center"><input type="checkbox" class="flat keja" name="data_irs[decub][status]" value="Ya"></td>
								<td><input type="text" name="data_irs[decub][date]" class="form-control kejaHS" disabled></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>inf WSD</th>
								<td class="text-center"><input type="checkbox" class="flat keja" name="data_irs[inf_wsd][status]" value="Ya"></td>
								<td><input type="text" name="data_irs[inf_wsd][date]" class="form-control kejaHS" disabled></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="form-group"></div>
				<div class="ln_solid" style="margin-top: -30px;"></div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12 text-center">
						<a href="{{ Route('hais.create') }}" class="btn btn-danger">Create Surveilans</a>
						<button type="submit" class="btn btn-success">Submit</button>
					</div>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
@endsection

@section('footer_scripts')
<script>
	$(document).ready(function(){
		
		$('input[type=checkbox]').on('ifChecked', function(event){
			$(this).closest('tr').find('input[type=text]').attr('disabled', false);
		}).on('ifUnchecked', function(event){
			$(this).closest('tr').find('input[type=text]').val('');
			$(this).closest('tr').find('input[type=text]').attr('disabled', true);
		});
		
		$('.startDate, .kejaHS').daterangepicker({
			singleDatePicker: true,
			singleClasses: "picker_2",
			locale: {
			  format: 'YYYY-MM-DD'
			}
		}, function(start, end, label) {
			
		}).val('');
		
		$('.endDate').daterangepicker({
			singleDatePicker: true,
			singleClasses: "picker_2",
			locale: {
			  format: 'YYYY-MM-DD'
			}
		}, function(start, end, label) {
			
		}).val('');
		
		$('.endDate').on('change', function(){
			var $start = $(this).closest('tr').find('input.startDate').val();
			
			if($start !== ''){
				countDate = moment($(this).val()).diff(moment($start), 'days')+1;
				$(this).closest('tr').find('input:last').val(countDate);
			}
		});
		
		$("input[name=txt_search]").typeahead({
			source: function(query, result)
			{
				$.ajax({
					url:'{{ route("getDataPasien") }}',
					method:"get",
					data:{'term': query, 'inap': 'Y'},
					dataType:"json",
					success:function(data)
					{
						result($.map(data, function(item){
							return item;
						}));
					}
				})
			},
			updater: function(obj) {
				
				return obj;
			},
			afterSelect: function (item) {
				$('input[name=no_reg]').val(item.no_reg);
				$('input[name=\'data_kunjungan[lp]\'][value='+item.lp+']').iCheck('check');
				setValueHais(item.no_reg);
				
				$.each(item, function (index, value) {
					value = (value == null || value == '') ? '-' : value;
					if(index != 'lp') $('input[name=\'data_kunjungan['+index+']\']').val(value);
				});
			},
			displayField: 'name'
		});
		
		$("input.icd").typeahead({
			source: function(query, result)
			{
				$.ajax({
					url:'{{ route("getICD") }}',
					method:"get",
					data:{'term': query},
					dataType:"json",
					success:function(data)
					{
						result($.map(data, function(item){
							return item;
						}));
					}
				})
			},
			updater: function(obj) {
				
				return obj;
			},
			afterSelect: function (item) {
				//
			},
			displayField: 'name'
		});
		
	});
	
	function setValueHais(no_reg){
		$.ajax({
			url:'{{ route("getDataHAIs") }}',
			method:"get",
			data:{'term': no_reg},
			dataType:"json",
			success:function(data)
			{
				console.log(data);
				if(data !== null){
					//data kunjungan
					$.each(data.data_kunjungan, function (index, value) {
						value = (value == null || value == '') ? '-' : value;
						if(index != 'lp') $('input[name=\'data_kunjungan['+index+']\']').val(value);
					});
					
					//data medis
					var $arr = ['hb','ht','gds','trombosit','leukosit'];
					$.each(data.data_medis, function (index, value) {	
						if($.inArray(index, $arr) !== -1){
							$('input[name=\'data_medis['+index+']\']').val(value);
						}else{
							$('input[name=\'data_medis['+index+']\'][value=\''+value+'\']').iCheck('check');
						}
					});
					
					//data pemasangan
					$.each(data.data_pemasangan, function (index, value) {
						$.each(value, function(i, v){
							if(i == 'status'){
								$('input[name=\'data_pemasangan['+index+']['+i+']\']').iCheck('check');
							}else{
								$('input[name=\'data_pemasangan['+index+']['+i+']\']').val(v);
							}
						});
					});
					
					//data kejadian irs
					$.each(data.data_irs, function (index, value) {
						$.each(value, function(i, v){
							if(i == 'status'){
								$('input[name=\'data_irs['+index+']['+i+']\']').iCheck('check');
							}else{
								$('input[name=\'data_irs['+index+']['+i+']\']').val(v);
							}
						});
					});
				}
			}
		});
	}
</script>
@endsection